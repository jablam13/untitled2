﻿App.Games = (function () {
    function Games(games, hub) {
        var patterns = {
            email: /^([\d\w-\.]+@([\d\w-]+\.)+[\w]{2,4})?$/,
            phone: /^\d[\d -]*\d$/,
            postcode: /^([a-zA-Z]{1,2}[0-9][0-9]?[a-zA-Z\s]?\s*[0-9][a-zA-Z]{2,2})|(GIR 0AA)$/
        };

        var self = this;
        self.hub = hub;
        self.GameName = ko.observable('');
        self.FirstName = ko.observable('');
        self.LastName = ko.observable('');
        self.Name = ko.computed(function () {
            return self.FirstName() + ' ' + self.LastName()
        });
        self.EmailAddress = ko.observable('').trim().extend({
            email: true,
            required: true,
            rateLimit: 1000
        });
        self.Sports = ko.observableArray(ko.utils.arrayMap(games.Sports, function (sport) { return new App.Sport(sport); }));
        self.Public = ko.observable(games.Public);
        self.Active = ko.observable(games.Active);
        self.AMPM = ko.observableArray(['PM', 'AM']);
        self.selectedAMPM = ko.observable();
        self.StartDate = ko.observable().extend({ required: true });
        self.StartHour = ko.observable(Hours());
        self.selectedHour = ko.observable();
        self.StartMinute = ko.observable(Minutes());
        self.selectedMinute = ko.observable();
        self.selectedSport = ko.observable();
        self.StartTime = ko.computed(function () {
            return self.StartDate() + ' ' + self.selectedHour() + ':' + self.selectedMinute() + ' ' + self.selectedAMPM();
        });
        self.GameList = ko.observableArray(ko.utils.arrayMap(games.Games, function (game) { return new App.Game(game, self.EmailAddress, self.FirstName, self.LastName, hub); }));
        self.CreatedDate = ko.observable(games.CreatedDate);
        self.hub.client.addGame = function (game, player) {
            var mappedGame = new App.Game(game, self.EmailAddress, self.FirstName, self.LastName, hub);
            if (mappedGame.EmailAddress() == player.Username) { mappedGame.CurrentlyPlaying(1); }
            self.GameList.unshift(mappedGame);
        };
        self.hub.client.updateCount = function (game, username) {
            var updatedGame = ko.utils.arrayFirst(self.GameList(), function (g) {
                return g.Id() == game.Id; // <-- is this the desired seat?
            });

            // Seat found?
            if (updatedGame) {
                // Update the "Booked" property of this seat!
                updatedGame.Count(game.Count);
                if (self.EmailAddress() == username) {
                    updatedGame.CurrentlyPlaying(game.CurrentlyPlaying);
                }
            }
        };
        self.EmailAddress.subscribe(function () {
            if (self.EmailAddress.isValid()) {
                data = {
                    Email: self.EmailAddress()
                };

                //add postback to update the games based off of user email
                $.ajax({
                    type: 'POST',
                    url: '/game/updateGames',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    traditional: true,
                    data: JSON.stringify(data)
                }).done(function (data, textStatus, xhr) {
                    self.GameList(ko.utils.arrayMap(data, function (game) {
                        return new App.Game(game, self.EmailAddress, self.FirstName, self.LastName, self.hub);
                    }));
                }).fail(function (xhr, textStatus, errorThrown) {
                    console.log(xhr);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
            }
        });
    }



    Games.prototype.changeEmail = function () {
        var self = this;
        self.EmailAddress('');
        self.GameName('');
        self.StartDate('');
        ko.utils.arrayForEach(self.GameList(), function (game) {
            game.CurrentlyPlaying(0);
        });

    };

    Games.prototype.addGame = function () {
        var self = this;
        if (self.EmailAddress.isValid() && self.StartDate.isValid()) {
            var result = !self.Public() ? 1 : 0;
            var player = {
                FirstName: self.FirstName(),
                LastName: self.LastName(),
                Username: self.EmailAddress()
            },
            game = {
                Name: self.GameName(),
                Public: result,
                StartTime: self.StartTime(),
                SportId: self.selectedSport()
            };
            self.hub.server.addGame(game, player);
        }
    };

    return Games;
})();

App.Game = (function () {
    function Game(game, email, firstName, lastName, hub) {
        var self = this;
        self.hub = hub;
        self.EmailAddress = email;
        self.FirstName = firstName;
        self.LastName = lastName;
        self.Id = ko.observable(game.Id)
        self.Name = ko.observable(game.Name);
        self.Sport = ko.observable(game.Sport);
        self.StartTime = ko.observable(moment(game.StartTime).format("YYYY-MM-DD hh:mm A"));
        self.Public = ko.observable(true);
        self.Count = ko.observable(game.Count);
        self.CurrentlyPlaying = ko.observable(game.CurrentlyPlaying);
        self.Active = ko.observable(true);
        self.CreatedDate = ko.observable(game.CreatedDate);
        self.JoinLeaveText = ko.computed(function () {
            if (self.CurrentlyPlaying() == 1) {
                return 'Leave';
            } else {
                return 'Join';
            }
        });
    }

    Game.prototype.JoinLeaveGame = function () {
        var self = this;
        //return, update Count, CurrentlyPlaying
        if (self.EmailAddress.isValid()) {
            var data = {};
            data.player = {
                FirstName: self.FirstName(),
                LastName: self.LastName(),
                Username: self.EmailAddress()
            };
            data.gameId = self.Id();

            self.hub.server.joinLeaveGame(data.player, data.gameId);
            if (self.CurrentlyPlaying() == 1) {
                self.CurrentlyPlaying(0);
            } else {
                self.CurrentlyPlaying(1);
            }
        }
    };

    return Game;
})();
App.Sport = (function () {
    function Sport(sport) {
        var self = this;
        self.Id = ko.observable(sport.Id);
        self.Name = ko.observable(sport.Name);
    }

    return Sport;
})();

//App.GameType = (function () {
//    function GameType(gameType) {
//        self = this;
//        self.Id = ko.observable(gameType.Id);
//        self.Name = ko.observable(gameType.Name);
//        self.Description = ko.observable(gameType.Description);
//    }

//    return GameType;
//})();

function Hours() {
    var hour = [];
    for (var i = 0; i < 12; i++) {
        hour[i] = i + 1;
    }
    return hour;
}
function Minutes() {
    var min = [];
    for (var i = 0; i < 4; i++) {
        min[i] = i * 15;
        if (i == 0) { min[i] = '00'; }
    }
    return min;
}