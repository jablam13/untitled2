﻿App.CoC = (function () {
    function CoC(cocSettings) {
        self = this;
        self.Name = ko.observable('Stores')
        self.Troops = ko.observableArray(ko.utils.arrayMap(cocSettings.Troops, function (troop) { return new App.Troop(troop); }));
    }

    return CoC;
})();

App.Troop = (function () {
    function Troop(troop) {
        var self = this;
        self.Id = ko.observable(store.Id);
        self.Name = ko.observable(troop.Name);
        self.UnitCost = ko.observable(troop.UnitCost);
        self.Elixer = ko.observable(troop.Elixer);
        self.Description = ko.observable(troop.Description);
        self.LargeImg = ko.observable(troop.LargeImg);
        self.SmallImg = ko.observable(troop.SmallImg);
        self.Active = ko.observable(troop.Active);
        self.Categories = ko.observableArray(ko.utils.arrayMap(troop.Categories, function (category) {
            return new App.Category(category, self);
        }));
        self.ItemList = ko.observable();
    };

    //Store.prototype.toggleUserMenu = function () {

    //};

    //Store.prototype.logoutClick = function () {

    //};

    return Troop;
})();