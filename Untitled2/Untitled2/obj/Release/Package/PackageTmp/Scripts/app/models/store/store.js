﻿App.Stores = (function () {
    function Stores(stores) {
        self = this;
        self.Name = ko.observable('Stores')
        self.Stores = ko.observableArray(ko.utils.arrayMap(stores.StoreList, function (store) { return new App.Store(store, self); }));
    }

    return Stores;
})();

App.Store = (function () {
    function Store(store, stores) {
        var self = this;
        self.Id = ko.observable(store.Id);
        self.Uid = ko.observable(store.Uid);
        self.CreatorId = ko.observable(store.CreatorId);
        self.CreatorName = ko.observable(store.CreatorName);
        self.Name = ko.observable(store.Name);
        self.Description = ko.observable(store.Description);
        self.LargeImg = ko.observable(store.LargeImg);
        self.SmallImg = ko.observable(store.SmallImg);
        self.Active = ko.observable(store.Active);
        self.Categories = ko.observableArray(ko.utils.arrayMap(store.Categories, function (category) {
            return new App.Category(category, self);
        }));
        self.ItemList = ko.observable();
    };

    //Store.prototype.toggleUserMenu = function () {

    //};

    //Store.prototype.logoutClick = function () {

    //};

    return Store;
})();