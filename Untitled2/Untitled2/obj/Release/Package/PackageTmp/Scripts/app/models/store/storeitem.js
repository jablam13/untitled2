﻿App.StoreItem = (function () {
    function StoreItem(item, category) {
        var self = this;
        self.Id = ko.observable(item.Id);
        self.Uid = ko.observable(item.Uid);
        self.StoreItemId = ko.observable(item.StoreItemId);
        self.StoreItemUid = ko.observable(item.StoreItemUid);
        self.StoreId = ko.observable(item.StoreId);
        self.CategoryId = ko.observable(item.CategoryId);
        self.Name = ko.observable(item.Name);
        self.Description = ko.observable(item.Description);
        self.Price = ko.observable(item.Price);
        self.Quantity = ko.observable(item.Quantity);
        self.TotalQuantity = ko.observable(item.TotalQuantity);
        self.selectQuantity = ko.observableArray(TotalQuantity(item.Quantity));
        self.selectedQuantity = ko.observable();
        self.LargeImg = ko.observable(item.LargeImg);
        self.SmallImg = ko.observable(item.SmallImg);
        self.Active = ko.observable(item.Active);
        self.Cart = ko.contextFor(document.getElementById("cart")).$root;
    }

    StoreItem.prototype.addToCart = function () {
        var self = this, item = {};
        item.Id = self.Id();
        item.Uid = self.Uid();
        item.StoreId = self.StoreId();
        item.Quantity = self.selectedQuantity();
        item.TotalQuantity = self.Quantity();
        item.Price = self.Price();
        item.CartTransaction = 'ADD';

        $.ajax({
            type: 'POST',
            url: '/cart/transaction',
            contentType: 'application/json',
            data: JSON.stringify(item),
            dataType: 'json'
        }).done(function (data) {
            if (data.Id != -1) {
                if ($('#cart').length) {
                    self.Cart.TotalPrice(data.TotalPrice);
                    self.Cart.Quantity(data.Quantity);
                    self.Quantity(data.CartItems[0].Quantity);
                    self.selectQuantity((TotalQuantity(data.CartItems[0].Quantity)));
                }
            } else {
                self.Cart.TotalPrice(data.TotalPrice);
                self.Cart.Quantity(data.Quantity);
                console.log(data)
            }
        });
    };

    return StoreItem;
})();

function TotalQuantity(quantity) {
    var quantityArray = []
    if (quantity < 0) {
        quantityArray[0] = 0
    } else {
        for (var i = 0; i <= quantity; i++) {
            quantityArray[i] = i;
        }
    }
    return quantityArray;
}