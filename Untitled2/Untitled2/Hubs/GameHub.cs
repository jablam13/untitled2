﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Untitled2.Models;
using Untitled2.Repository;

namespace Untitled2.Hubs
{
    [HubName("game")]
    public class GameHub : Hub
    {

        private readonly GameRepository rep = new GameRepository();

        public void AddGame(Game game, Player player)
        {
            var addedGame = rep.AddGame(player, game);

            Clients.All.addGame(addedGame, player);
        }
        public void JoinLeaveGame(Player player, int gameId)
        {
            var count = rep.JoinLeaveGame(player, gameId);

            Clients.All.UpdateCount(count, player.Username);
        }
    }
}