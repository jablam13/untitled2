﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Untitled2.Helpers
{
    public static class WebViewPageExtensions
    {
        public static IHtmlString GetPageTitle(this WebViewPage page)
        {
            var title = (string)page.ViewData["PageTitle"];
            title = (title.IsNullOrEmpty() ? "" : title);
            return new HtmlString(title);
        }

        public static void SetPageTitle(this WebViewPage page, string title)
        {
            page.ViewData["PageTitle"] = HttpUtility.HtmlEncode(title);
        }
    }

    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool HasValue(this string str)
        {
            return !str.IsNullOrEmpty();
        }
    }

    public static class NumberExtensions
    {
        public static bool IsInt(this decimal number)
        {
            return number % 1 == 0; // ehhh close enough?
        }
    }

    public static class ObjectExtensions
    {
        public static IHtmlString ToJson(this object obj)
        {
            return
                new HtmlString(JsonConvert.SerializeObject(obj,
                    new JsonSerializerSettings()
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        NullValueHandling = NullValueHandling.Ignore
                    }));
        }
    }

    public static class HtmlHelperExtensions
    {
        private static IHtmlString InputField(this HtmlHelper helper, string id, string viewModelProperty, string placeholderText, string type = "text", Dictionary<string, string> attributes = null)
        {
            if (attributes != null)
            {
                attributes.Add("data-title", placeholderText);
            }

            var additionalAttributes = attributes != null ? string.Join(" ", attributes.Select(a => string.Format("{0}='{1}'", a.Key, a.Value))) : "";

            var html = @"
<div class=""field"">
    <div class=""labelWrapper"" style=""float:left;""><div>{2}</div></div>
    <div class=""inputWrapper"" style=""float:left;"">
        <input id=""{0}"" name=""{0}"" type=""{3}"" data-bind=""value: {1}"" {4} />
    </div>
    <div style=""clear:both;""></div>
</div>
";
            var htmlString = string.Format(html, id, viewModelProperty, placeholderText, type, additionalAttributes);
            return new HtmlString(htmlString);
        }

        public static IHtmlString TextField(this HtmlHelper helper, string id, string viewModelProperty, string placeholderText, Dictionary<string, string> attributes = null)
        {
            return helper.InputField(id, viewModelProperty, placeholderText, "text", attributes);
        }

        public static IHtmlString PasswordField(this HtmlHelper helper, string id, string viewModelProperty, string placeholderText, Dictionary<string, string> attributes = null)
        {
            return helper.InputField(id, viewModelProperty, placeholderText, "password", attributes);
        }

        public static IHtmlString DropdownField(this HtmlHelper helper, string id, string placeholderText, string databind, Dictionary<string, string> attributes = null)
        {
            if (attributes != null)
            {
                attributes.Add("data-title", placeholderText);
            }

            var additionalAttributes = attributes != null ? string.Join(" ", attributes.Select(a => string.Format("{0}='{1}'", a.Key, a.Value))) : "";
            var html = @"
<div class=""field"">
<div class=""labelWrapper""><div class=""label"">{2}</div></div>
    <div class=""inputWrapper"">
        <select id=""{0}"" name=""{0}"" data-bind=""{1}"" {3}></select>
    </div>
</div>
";
            var htmlString = string.Format(html, id, databind, placeholderText, additionalAttributes);
            return new HtmlString(htmlString);
        }

        public static IHtmlString ContentSection(this HtmlHelper helper, string id, string title, IHtmlString html, bool isOpen)
        {
            var htmlTemplate = @"
<div id=""{0}"" class=""contentSection"" data-bind=""toggleContent: {1}"">
    <div class=""headerWrapper"">
        <div class=""header"" data-bind=""style: {{ color: color }}"">
            <div class=""title"">{2}</div>
        </div>
    </div>
    <div class=""content"">
        {3}
    </div>
</div>
";

            var htmlString = string.Format(htmlTemplate, id, isOpen.ToJson(), title, html);
            return new HtmlString(htmlString);
        }
        public static string CurrentViewName(this HtmlHelper html)
        {
            return System.IO.Path.GetFileNameWithoutExtension(
                ((RazorView)html.ViewContext.View).ViewPath
            );
        }
    }
}