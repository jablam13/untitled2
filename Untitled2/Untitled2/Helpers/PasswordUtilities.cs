﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Untitled2.Helpers
{
    public class PasswordUtilities
    {
        private static readonly byte SaltLength = 24;
        private static readonly int Iterations = 4000;
        private static readonly byte KeyLength = 24;
        private static readonly RandomNumberGenerator RandomSource = RandomNumberGenerator.Create();

        public static string GenerateHash(string value, string salt)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(value), Convert.FromBase64String(salt), Iterations);

            var hash = pbkdf2.GetBytes(KeyLength);

            // SQL CLR is .NET2 vs Wombat as .NET4 and there was a change to Rfc2898DeriveBytes to be a disposable in .NET4
            if (pbkdf2 is IDisposable)
            {
                ((IDisposable)pbkdf2).Dispose();
            }

            return Convert.ToBase64String(hash);
        }

        public static string GenerateSalt()
        {
            var salt = Random(SaltLength);
            return Convert.ToBase64String(salt);
        }

        private static byte[] Random(int bytes)
        {
            var ret = new byte[bytes];
            lock (RandomSource)
            {
                RandomSource.GetBytes(ret);
            }
            return ret;
        }
    }
}