﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Untitled2.Helpers
{
    public class PasswordComplexityAttribute : ValidationAttribute
    {
        public int MinLength { get; set; }
        public int RequiredUppercaseCharacters { get; set; }
        public int RequiredLowercaseCharacters { get; set; }
        public int RequiredNumberCharacters { get; set; }
        public int RequiredSpecialCharacters { get; set; }

        public PasswordComplexityAttribute()
        {
            ErrorMessage = "Your password...";
        }

        public override bool IsValid(object value)
        {
            var passwordAttempt = (string)value;

            var isValid = passwordAttempt != null &&
                passwordAttempt.Length >= MinLength &&
                passwordAttempt.Count(char.IsUpper) >= RequiredUppercaseCharacters &&
                passwordAttempt.Count(char.IsLower) >= RequiredLowercaseCharacters &&
                passwordAttempt.Count(char.IsNumber) >= RequiredNumberCharacters &&
                passwordAttempt.Count(c => char.IsSymbol(c) || char.IsPunctuation(c)) >= RequiredSpecialCharacters;

            return isValid;
        }

        public override string FormatErrorMessage(string name)
        {
            return ErrorMessage
                .Replace("{{MinLength}}", MinLength.ToString())
                .Replace("{{RequiredUppercaseCharacters}}", RequiredUppercaseCharacters.ToString())
                .Replace("{{RequiredLowercaseCharacters}}", RequiredLowercaseCharacters.ToString())
                .Replace("{{RequiredNumberCharacters}}", RequiredNumberCharacters.ToString())
                .Replace("{{RequiredSpecialCharacters}}", RequiredSpecialCharacters.ToString());
        }
    }

    public class MustEqualProperty : ValidationAttribute
    {
        public string OtherPropertyName { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(OtherPropertyName);
            var otherPropertyValue = property.GetValue(validationContext.ObjectInstance);

            return value != null && value.Equals(otherPropertyValue) ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName });
        }
    }
}