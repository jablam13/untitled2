﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class GameModel
    {
        public List<Sport> Sports { get; set; }
        public List<Game> Games { get; set; }
    }
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public GameType Type { get; set; }
        public int SportId { get; set; }
        public string Sport { get; set; }
        public DateTime StartTime { get; set; }
        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public int Public { get; set; }
        public int Active { get; set; }
        public int Count { get; set; }
        public int CurrentlyPlaying { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class GameType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Sport
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}