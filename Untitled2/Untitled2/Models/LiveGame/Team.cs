﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CoachId { get; set; }
        public string Coach { get; set; }
        public int CreatorId { get; set; }
        public string Creator { get; set; }
        public List<Player> Players { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}