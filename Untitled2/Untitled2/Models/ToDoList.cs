﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class ToDoList
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<ToDoListItem> ListItems { get; set; }
        public string CreatedDate { get; set; }
        public string LastModifiedDate { get; set; }
    }

    public class ToDoListItem
    {
        public int Id { get; set; }
        public int ListId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string LastModifiedDate { get; set; }
    }
}