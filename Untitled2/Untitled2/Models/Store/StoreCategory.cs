﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class StoreCategory
    {

        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int StoreId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LargeImg { get; set; }
        public string SmallImg { get; set; }
        public List<StoreItem> StoreItems { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<string> Errors { get; set; }

    }
}