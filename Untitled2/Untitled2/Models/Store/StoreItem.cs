﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class StoreItem
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int StoreItemId { get; set; }
        public Guid StoreItemUid { get; set; }
        public int StoreId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public int TotalQuantity { get; set; }
        public string LargeImg { get; set; }
        public string SmallImg { get; set; }
        public int Active { get; set; }
        public int Status { get; set; }
        public string CartTransaction { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<string> Errors { get; set; }
    }
}