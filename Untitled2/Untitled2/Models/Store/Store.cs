﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class Stores
    {
        public List<Store> StoreList { get; set; }
        public List<string> Errors { get; set; }
    }
    public class Store
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int CreatorId { get; set; }
        public string CreatorName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LargeImg { get; set; }
        public string SmallImg { get; set; }
        public List<StoreCategory> Categories { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<string> Errors { get; set; }

    }
}