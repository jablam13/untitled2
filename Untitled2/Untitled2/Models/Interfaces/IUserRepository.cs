﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public interface IUserRepository
    {
        User Get(int userId);
        User Get(string email);
        User Register(User user, string password);
        bool IsUsernameAvailable(string email);
        AuthLoginAttempt GetParticipantByUsername(string email);
    }
}