﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models.Map
{
    public class Places
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public Guid Uid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Active { get; set; }
        public int NestedId { get; set; }
        public List<Place> PlaceList { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }

    public class Place
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public Guid ListUid { get; set; }
        public int ListId { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GooglePlaceId { get; set; }
        public string icon { get; set; }
        public string place_id { get; set; }
        public string GoogleName { get; set; }
        public int price_level { get; set; }
        public decimal rating { get; set; }
        public string vicinity { get; set; }
        public string formatted_address { get; set; }
        public string international_phone_number { get; set; }
        public int permanently_closed { get; set; }
        public string website { get; set; }
        public int Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}