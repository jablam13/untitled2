﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class User
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int UserId { get; set; }
        public Guid UserUid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public int IsAdmin { get; set; }
        public int LeagueAdmin { get; set; }
        public int TeamAdmin { get; set; }
        public string OpenId { get; set; }
        public string OpenIdClaim { get; set; }
        public Guid Validator { get; set; }
        public int InActive { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
    }
}