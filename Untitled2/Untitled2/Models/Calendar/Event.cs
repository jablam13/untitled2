﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models.Calendar
{
    public class Event
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int SiteId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Duration { get; set; }
        public int IsAttending { get; set; }
        public List<Attendee> Attendees { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}