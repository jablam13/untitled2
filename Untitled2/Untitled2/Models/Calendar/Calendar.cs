﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models.Calendar
{
    public class Calendar
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public string Month { get; set; }
        public List<Day> Days { get; set; }
    }
}