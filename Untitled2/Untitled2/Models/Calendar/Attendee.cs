﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models.Calendar
{
    public class Attendee
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public Guid UserUid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IsGoing { get; set; }
    }
}