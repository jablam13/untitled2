﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models.Calendar
{
    public class Day
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public DateTime Date { get; set; }
        public List<Event> Events { get; set; }
    }
}