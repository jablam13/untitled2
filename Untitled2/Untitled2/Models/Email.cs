﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Untitled2.Models;
using Untitled2.Repository;
using System.Net;
using SendGrid;
using System.Net.Mime;

namespace Untitled2
{
    public class Email
    {
        public void SendEmail(User registrant, string subject)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();

                // To
                mailMsg.To.Add(new MailAddress(registrant.EmailAddress.Trim(), registrant.FirstName.Trim() + " " + registrant.LastName.Trim()));

                // From
                mailMsg.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], ConfigurationManager.AppSettings["sendGridFromName"]);

                // Subject and multipart/alternative Body
                mailMsg.Subject = "subject";
                string text = subject;
                string html = subject;
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"], Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["sendGridUserName"], ConfigurationManager.AppSettings["sendGridPassword"]);
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool SendEmail(string Subject, string Message, string Recipient, bool SendAsText)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();

                // To
                mailMsg.To.Add(new MailAddress(Recipient.Trim(), ""));

                // From
                mailMsg.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], ConfigurationManager.AppSettings["sendGridFromName"]);

                // Subject and multipart/alternative Body
                mailMsg.Subject = Subject;
                string text = Message.Trim();
                string html = Message.Trim();
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"], Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["sendGridUserName"], ConfigurationManager.AppSettings["sendGridPassword"]);
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return false;
            }

            return false;
        }
    }
}