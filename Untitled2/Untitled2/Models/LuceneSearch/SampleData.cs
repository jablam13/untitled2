﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Untitled2.Models
{
    public class SampleData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}