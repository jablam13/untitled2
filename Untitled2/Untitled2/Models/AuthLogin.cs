﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Helpers;
using Untitled2.Models;

namespace Untitled2.Models
{
    public class AuthLoginAttempt
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
        public Guid Uid { get; set; }
        public Guid UserUid { get; set; }
    }
    public class ForgotPassword
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public int UserId { get; set; }
        public Guid UserUid { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public string StatusMessage { get; set; }
        public List<string> Errors { get; set; }
        public Dictionary<string, List<string>> FormFieldErrors { get; set; }
    }
}