﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models;
using Untitled2.Repository;
using Untitled2.Helpers;
using System.Security.Claims;

namespace Untitled2.Controllers
{
    public class StoresController : Controller
    {
        private readonly StoreRepository rep = new StoreRepository();

        // GET: Stores
        [Route("stores")]
        public ActionResult Index()
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            Stores stores = new Stores();

            stores.StoreList = rep.GetStores(id);

            return View(stores);
        }
    }
}