﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Untitled2.Repository;
using Untitled2.Models.Map;
using System.Web;
using System.Security.Claims;


namespace Untitled2.Controllers
{
    public class MapsController : ApiController
    {
        private readonly MapRepository gm = new MapRepository();

        [Route("maps/getlists")]
        [HttpGet]
        public List<Places> Lists()
        {

            int id = 0;

            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            var lists = gm.GetLists(id);
            if (lists == null) throw new HttpResponseException(HttpStatusCode.NotFound);
            return lists;
        }

        [Route("maps/addList")]
        [HttpPost]
        public Places AddList(Places list)
        {

            int id = 0;

            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (id != 0)
            {
                return gm.AddList(list, id);
            }
            else
            {
                return new Places();
            }
        }

        [Route("maps/addItem")]
        [HttpPost]
        public Place AddItem(Place place)
        {

            int id = 0;

            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }
            return gm.AddItem(place, id);
        }

        [Route("maps/removelist")]
        [HttpPost]
        public int RemoveList(Place place)
        {

            int id = 0;

            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }
            if (id != 0)
            {
                gm.RemoveList(place.ListId, id);
            }
            return 1;
        }

        [Route("maps/removeplace")]
        [HttpPost]
        public Place RemovePlace(Place place)
        {
            int userId = Convert.ToInt16(HttpContext.Current.User.Identity.Name);
            return gm.RemovePlace(place.Id, place.GooglePlaceId);
        }
    }
}