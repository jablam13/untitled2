﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models;
using Untitled2.Repository;
using Untitled2.Helpers;
using System.Threading.Tasks;
using Untitled2.Views.Account;
using System.Web.Security;
using System.Web.UI;
using System.Threading;

namespace Untitled2.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserRepository rep = new UserRepository();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [Route("account/register")]
        [HttpGet]
        public ActionResult Register()
        {
            var viewModel = new RegisterViewModel();

            return View(viewModel);
        }

        [Route("account/register")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "FirstName,LastName,EmailAddress,Password,PasswordRpt")] RegisterViewModel viewModel)
        {
            Email email = new Email();
            var errors = new List<string>();

            if (!ModelState.IsValid)
            {
                foreach (var field in ModelState)
                {
                    foreach (var error in field.Value.Errors)
                    {
                        var errorMessage = error.ErrorMessage;
                        errors.Add(errorMessage);
                    }
                }
            }

            var user = new User()
            {
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName,
                EmailAddress = viewModel.EmailAddress
            };

            user = rep.Register(user, viewModel.Password);

            if (user != null)
            {
                if (user.InActive == 0)
                {
                    errors.Add("This username already exists.");
                }

                if (errors.Any())
                {
                    viewModel.Errors = errors;
                    return View(viewModel);
                }

                if (user.InActive == 1)
                {
                    var subject = ValidatorMessage(user.Validator, "validate");

                    email.SendEmail(user, subject);

                    return RedirectToAction("RegistrationPending", "Account", null);
                }
            }

            return RedirectToAction("Index", "Home", null);
        }


        [Route("account/registrationpending")]
        public ActionResult RegistrationPending()
        {
            return View();
        }


        [Route("account/validate/{id}")]
        public ActionResult Validate(Guid? id)
        {
            //dynamic model = new { Message = "" };

            if (id != null)
            {
                var user = rep.ValidateEmail(id);

                if (user == null)
                {
                    ViewData["Message"] = "Invalid email validator id.";
                }
                else
                {

                    ViewData["Message"] = "Your email address has been validated.";

                    IdentitySignin(user.Id.ToString(), user.FirstName + ' ' + user.LastName, user.Id.ToString());
                }
            }
            return View();
        }

        public string ValidatorMessage(Guid? validator, string action)
        {
            return string.Format(
                @"<div><div>In order to validate your email address for CodePaste.net, please click or paste the
following link into your browser's address bar: </div>
<div>
<a href='{0}'>{0}</a>
</div><div>
Once you click the link you're ready to create new
code pastes with your account on the Zub site.
</div><div>
Sincerely,
</div><div>
The Zub Team</div></div>
", /*new Uri().AbsoluteUri*/

            new System.Uri(Request.Url.Authority + "/account/" + action + "/" + validator.ToString()).AbsoluteUri
            );
        }

        [Route("account/login")]
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View("", "", new LoginViewModel());
        }
        private bool LogInUser(string username, string password)
        {
            var authLoginAttempt = rep.GetParticipantByUsername(username);

            if (authLoginAttempt != null && authLoginAttempt.Hash == PasswordUtilities.GenerateHash(password, authLoginAttempt.Salt))
            {
                //var user = rep.Get(authLoginAttempt.Id);
                //FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
                return true;
            }

            return false;
        }

        //public JsonResult Login([Bind(Include = "Username, Password")] LoginViewModel viewModel)
        //{ 

        //}

        [Route("account/login")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Username, Password")] LoginViewModel viewModel)
        {
            // validate your user 
            if (ModelState.IsValid)
            {
                var success = LogInUser(viewModel.Username, viewModel.Password);

                if (success)
                {
                    var user = rep.Get(viewModel.Username);
                    if (user != null)
                    {
                        IdentitySignin(user.UserId.ToString(), user.FirstName + ' ' + user.LastName, user.OpenId, viewModel.RememberMe);
                    }

                    var returnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["r"];
                    if (returnUrl != null)
                    {
                        if (returnUrl.StartsWith(Url.Content("~")))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("~" + returnUrl);
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }

                viewModel.ErrorMessage = "You have entered an invalid username or password.";
            }
            else
            {
                viewModel.FormFieldErrors = ModelState.Select(ms => new { Key = ms.Key, Errors = ms.Value.Errors.Select(e => e.ErrorMessage).ToList() }).ToDictionary(e => e.Key, e => e.Errors);
            }

            return RedirectToAction("Login", "Account", viewModel);
        }

        [Route("account/logoff")]
        public ActionResult LogOff()
        {
            IdentitySignout();
            return Redirect(Request.UrlReferrer.ToString());
        }

        public void IdentitySignin(string userId, string name, string providerKey = null, bool isPersistent = false)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.NameIdentifier, userId));
            claims.Add(new Claim(ClaimTypes.Name, name));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            Thread.CurrentPrincipal = principal;

            // add to user here!
            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, identity);
        }


        [Route("Account/ResetPassword")]
        public ActionResult ResetPassword(ForgotPasswordViewModel viewModel)
        {
            Email email = new Email();
            var errors = new List<string>();

            if (ModelState.IsValid)
            {
                if (viewModel.Email != null || viewModel.Email.Trim() != "")
                {
                    // check and see if user exists. change to return user
                    var user = rep.GetNewValidator(viewModel.Email);

                    if (user != null)
                    {
                        //send email to reset password
                        var subject = ValidatorMessage(user.Validator, "ResetPassword");

                        email.SendEmail(user, subject);

                        return View(viewModel);
                    }
                }
                return View(viewModel);
            }
            else
            {
                foreach (var field in ModelState)
                {
                    foreach (var error in field.Value.Errors)
                    {
                        var errorMessage = error.ErrorMessage;
                        errors.Add(errorMessage);
                    }
                }
                viewModel.Errors = errors;
                viewModel.FormFieldErrors = ModelState.Select(ms => new { Key = ms.Key, Errors = ms.Value.Errors.Select(e => e.ErrorMessage).ToList() }).ToDictionary(e => e.Key, e => e.Errors);
                return View(viewModel);
            }
        }

        [Route("Account/ResetPassword/{guid}")]
        public ActionResult ResetPassword(Guid guid)
        {
            var errors = new List<string>();
            ForgotPasswordViewModel viewModel = new ForgotPasswordViewModel();

            if (!ModelState.IsValid)
            {
                foreach (var field in ModelState)
                {
                    foreach (var error in field.Value.Errors)
                    {
                        var errorMessage = error.ErrorMessage;
                        errors.Add(errorMessage);
                    }
                }
                viewModel.Errors = errors;

                return View(viewModel);
            }
            else
            {
                ForgotPassword model = new ForgotPassword();

                if (guid == null)
                {
                    model.StatusMessage = "The link you sent appears to be incorrect. Please try again.";
                    return View("ResetPassword", "Account", model);
                }
                else
                {
                    //check if guid exists. if does, show reset password fields
                    var user = rep.IsUsernameAvailable(guid);
                    if (user.Status == 1)
                    {
                        viewModel.Email = user.Email;
                        return View(viewModel);
                    }
                    else
                    {
                        viewModel.Errors.Add("This doesnt exist, or there was an error trying to find them. Please try again later.");

                        return View(viewModel);
                    }
                }
            }
        }


        [Route("account/userExists")]
        public JsonResult CheckUser(string email)
        {
            var updatedGames = rep.IsUserExists(email);
            return Json(updatedGames, JsonRequestBehavior.AllowGet);
        }

        public void IdentitySignout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                                          DefaultAuthenticationTypes.ExternalCookie);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

    }
}