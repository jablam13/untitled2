﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Untitled2.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        [Route("home")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("home/otherpage")]
        public ActionResult OtherPage()
        {
            return View();
        }

        [Route("home/audioplayer")]
        public ActionResult AudioPlayer()
        {
            return View();
        }

        [Route("home/asteroids")]
        public ActionResult Asteroids()
        {
            return View();
        }

        [Route("home/maps")]
        public ActionResult Maps()
        {
            return View();
        }
    }
}