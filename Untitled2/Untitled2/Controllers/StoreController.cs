﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models;
using Untitled2.Repository;
using Untitled2.Helpers;
using System.Security.Claims;

namespace Untitled2.Controllers
{
    public class StoreController : Controller
    {
        private readonly StoreRepository rep = new StoreRepository();

        [Route("store")]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Stores");
        }

        [Route("store/{guid}")]
        public ActionResult Index(Guid guid)
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }


            if (Session["__MySessionCart"] == null)
            {
                var newCart = new Cart();
                newCart = new Cart() { };
                newCart.Id = -1;
                newCart.Uid = new Guid();
                newCart.CartItems = new List<StoreItem>();

                Session["__MySessionCart"] = newCart;
            }

            var cart = (Cart)Session["__MySessionCart"];

            if (guid == null)
            {
                return RedirectToAction("Index", "Stores");
            }
            else
            {
                var stores = rep.GetStoreAll(id, guid);

                if (id == 0)
                {
                    foreach(StoreCategory category in stores.Categories)
                    {
                        foreach (StoreItem item in category.StoreItems)
                        {
                            var cartItem = cart.CartItems.Where(d => d.Uid == item.Uid).FirstOrDefault();
                            if (cartItem != null)
                            {
                                item.Quantity = item.TotalQuantity - cartItem.Quantity; 
                            }
                        }
                    }
                }

                if (stores != null)
                {
                    return View(stores);
                }
                else
                {
                    return RedirectToAction("Index", "Stores");
                }
            }
        }

        [Route("storeitem/{guid}")]
        public ActionResult StoreItem(Guid guid)
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (guid == null)
            {
                return RedirectToAction("Index", "Stores");
            }
            else
            {
                var storeItem = rep.GetStoreItem(id, guid);

                if (id == 0)
                {
                    var cart = (Cart)Session["__MySessionCart"];

                    var cartItem = cart.CartItems.Where(d => d.Uid == storeItem.Uid).FirstOrDefault();
                    if (cartItem != null)
                    {
                        storeItem.Quantity = storeItem.TotalQuantity - cartItem.Quantity;
                    }
                }

                if (storeItem != null)
                {
                    return View(storeItem);
                }
                else
                {
                    return RedirectToAction("Index", "Stores");
                }
            }
        }

        [Route("cart/transaction")]
        public JsonResult CartTransaction(StoreItem item)
        {
            int id = 0;
            Cart status = new Cart();
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (id != 0)
            {
                if (item == null)
                {
                    //redirect to current view
                }
                else
                {
                    //complete cart transaction
                    status = rep.CartTransaction(id, item);

                    //return updated cart and update cart partial view

                }

                return Json(status, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                //update session cart

                var cartReturn = new Cart();

                var returnCart = AnonymousCartUpdate(cartReturn, item);

                return Json(returnCart, JsonRequestBehavior.AllowGet);
            }
        }

        public Cart AnonymousCartUpdate(Cart cartReturn, StoreItem item)
        {
            var cart = (Cart)Session["__MySessionCart"];

            if(cart.CartItems == null)
            {
                cart.CartItems = new List<StoreItem>();
            }

            bool containsItem = cart.CartItems.Any(i => i.Uid == item.Uid);

            if (item.CartTransaction == "ADD")
            {
                if (containsItem)
                {
                    var cartItem = cart.CartItems.Where(d => d.Uid == item.Uid).FirstOrDefault();
                    if (cartItem != null)
                    {
                        if (item.Quantity + cartItem.Quantity > cartItem.TotalQuantity)
                        {
                            cartItem.Quantity = cartItem.TotalQuantity;
                        }
                        else
                        {
                            cartItem.Quantity = item.Quantity + cartItem.Quantity;
                        }
                    }
                }
                else
                {
                    cart.CartItems.Add(item);
                }


                cart.Quantity = cart.CartItems.Sum(i => i.Quantity);
                cart.TotalPrice = cart.CartItems.Sum(i => i.Price * i.Quantity);

                //Session["__MySessionCart"] = cart;

                cartReturn.Quantity = cart.Quantity;
                cartReturn.TotalPrice = cart.TotalPrice;

                cartReturn.CartItems = new List<StoreItem>();

                var returnItem = new StoreItem();
                returnItem.Quantity = item.TotalQuantity - item.Quantity;
                if (returnItem.Quantity < 0) { returnItem.Quantity = 0; }
                cartReturn.CartItems.Add(returnItem);

                return cartReturn;
            }
            else if (item.CartTransaction == "UPDATE")
            {
                var cartItem = cart.CartItems.Where(d => d.Uid == item.Uid).FirstOrDefault();

                if (containsItem)
                {
                    if (cartItem != null)
                    {
                        if (item.Quantity < 0)
                        {
                            cartItem.Quantity = 0;
                        }
                        else if(item.Quantity > (cartItem.TotalQuantity + cartItem.Quantity))
                        {
                            cartItem.Quantity = cartItem.TotalQuantity + cartItem.Quantity;
                        }
                        else 
                        {
                            cartItem.Quantity = item.Quantity;
                        }
                    }
                }

                cart.Quantity = cart.CartItems.Sum(i => i.Quantity);
                cart.TotalPrice = cart.CartItems.Sum(i => i.Price * i.Quantity);


                cartReturn.Quantity = cart.Quantity;
                cartReturn.TotalPrice = cart.TotalPrice;

                cartReturn.CartItems = new List<StoreItem>();

                var returnItem = new StoreItem();
                returnItem.Quantity = item.Quantity;
                returnItem.TotalQuantity = item.Quantity + item.TotalQuantity;
                if (returnItem.Quantity < 0) { returnItem.Quantity = 0; }
                cartReturn.CartItems.Add(returnItem);

                return cartReturn;
            }
            else if (item.CartTransaction == "REMOVE")
            {
                if (containsItem)
                {
                    var cartItem = cart.CartItems.Where(d => d.Uid == item.Uid).FirstOrDefault();
                    if (cartItem != null)
                    {
                        var setToRemove = new HashSet<StoreItem>(cart.CartItems);
                        cart.CartItems.RemoveAll(x => setToRemove.Contains(x));
                    }
                }

                cart.Quantity = cart.CartItems.Sum(i => i.Quantity);
                cart.TotalPrice = cart.CartItems.Sum(i => i.Price * i.Quantity);

                return cart;
            }
            else
            {
                return new Cart() { };
            }

            //return new Cart() { };
        }
    }
}