﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models;
using Untitled2.Repository;

namespace Untitled2.Controllers
{
    public class GamesController : Controller
    {
        // GET: Games
        private readonly GameRepository rep = new GameRepository();
        // GET: Games
        [Route("game")]
        public ActionResult Index()
        {
            var gameModel = new GameModel();

            gameModel = rep.GetGameModel("");
            return View(gameModel);
        }
        //User Adds Game
        [Route("game/addGame")]
        public JsonResult AddGame(Player player, Game game)
        {
            var gameAdded = rep.AddGame(player, game);
            return Json(gameAdded, JsonRequestBehavior.AllowGet);
        }
        //Games Get Updated on Changing User
        [Route("game/updateGames")]
        public JsonResult GetGames(string email)
        {
            var updatedGames = rep.GetGames(email);
            return Json(updatedGames, JsonRequestBehavior.AllowGet);
        }

        [Route("games/asteroid")]
        public ActionResult Asteroid()
        {
            return View();
        }

        ////Games Get Updated on Changing User
        //[Route("game/joinLeaveGame")]
        //public JsonResult GetGames(Player player, int gameId)
        //{
        //    var joinLeaveGame = rep.JoinLeaveGame(player, gameId);
        //    return Json(joinLeaveGame, JsonRequestBehavior.AllowGet);
        //}
    }
}