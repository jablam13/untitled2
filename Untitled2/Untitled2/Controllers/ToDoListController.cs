﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Repository;
using Untitled2.Models;
using System.Security.Claims;

namespace Untitled2.Controllers
{
    public class ToDoListController : Controller
    {
        private readonly ToDoListRepository tdl = new ToDoListRepository();
        //
        // GET: /ToDo/
        [Route("ToDoList")]
        public ActionResult Index()
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            return View(tdl.GetLists(id));
        }

        [Route("ToDoList/testview")]
        public ActionResult TestView()
        {
            return View();
        }

        [Route("ToDoList/List")]
        [HttpGet]
        public ActionResult List()
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            return View(tdl.GetLists(id));
        }

        [Route("ToDoList/List/UpdateList")]
        [HttpPost]
        public void UpdateLists(List<ToDoList> lists)
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            tdl.UpdateList(lists, id);
        }

        [Route("ToDoList/List/CancelUpdateList")]
        [HttpPost]
        public JsonResult GetList(int listId)
        {
            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            return Json(tdl.GetListItems(id, listId), JsonRequestBehavior.AllowGet);
        }

        [Route("ToDoList/list/getitem/{id}")]
        [HttpGet]
        public JsonResult GetItem(int id)
        {
            return Json(tdl.GetItem(id), JsonRequestBehavior.AllowGet);
        }

        [Route("ToDoList/testview/updatelist")]
        [Route("ToDoList/list/UpdateListItems")]
        [HttpPost]
        public int UpdateItem(List<ToDoListItem> list)
        {
            return tdl.UpdateItem(list);
        }

        [Route("ToDoList/list/additem")]
        [HttpPost]
        public JsonResult AddListItem(ToDoListItem list)
        {
            int newItemId = tdl.AddItem(list);
            return Json(tdl.GetItem(newItemId), JsonRequestBehavior.AllowGet);
        }

        [Route("ToDoList/list/addlist")]
        [HttpPost]
        public JsonResult AddList(ToDoList list)
        {
            int id = 0;
            ToDoList newList = new ToDoList() { };
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (id != 0)
            {
                newList = tdl.AddList(list, id);
            }
            return Json(newList, JsonRequestBehavior.AllowGet);
        }

        [Route("ToDoList/List/RemoveItem")]
        [HttpPost]
        public JsonResult RemoveItem(ToDoListItem item)
        {
            tdl.RemoveItem(item);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [Route("ToDoList/List/RemoveList")]
        [HttpPost]
        public JsonResult RemoveItem(ToDoList list)
        {
            tdl.RemoveList(list);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
    }
}