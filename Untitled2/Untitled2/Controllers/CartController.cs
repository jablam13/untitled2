﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models;
using System.Security.Claims;
using Untitled2.Repository;

namespace Untitled2.Controllers
{
    public class CartController : Controller
    {
        private readonly StoreRepository rep = new StoreRepository();

        // GET: Cart
        [Route("cart")]
        public ActionResult Index()
        {
            int id = 0;
            StoreItem status = new StoreItem();
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (id == 0)
            {
                var anonCart = new Cart();

                if (Session["__MySessionCart"] == null)
                {
                    anonCart = new Cart() { };
                    anonCart.Id = -1;
                    anonCart.Uid = new Guid();
                    anonCart.CartItems = new List<StoreItem>();

                    Session["__MySessionCart"] = anonCart;
                }

                return View(Session["__MySessionCart"]);
            }
            else
            {
                var cart = rep.GetCartAndItems(id);

                return View(cart);
            }
        }

        [Route("cart/summary")]
        public PartialViewResult Summary(Cart cart)
        {
            int id = 0;
            StoreItem status = new StoreItem();
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (id == 0)
            {
                var anonCart = new Cart();

                if (Session["__MySessionCart"] == null)
                {
                    anonCart = new Cart() { };
                    anonCart.Id = 0;
                    anonCart.Uid = Guid.NewGuid();
                    anonCart.CartItems = new List<StoreItem>();

                    Session["__MySessionCart"] = anonCart;
                }
                var cart1 = new Cart();
                var sCart = (Cart)Session["__MySessionCart"];

                cart1.Quantity = sCart.Quantity;
                cart1.TotalPrice = sCart.TotalPrice;

                return PartialView(cart1);
            }
            else
            {
                ViewData["myCart"] = rep.GetCart(id);
                return PartialView(ViewData["myCart"]);
            }
        }

        [Route("cart/cartdetails")]
        public JsonResult CartDetails()
        {
            int id = 0;
            StoreItem status = new StoreItem();
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }
            var cart = rep.GetCart(id);

            if (id == 0)
            {
                if (cart == null)
                {
                    cart = (Cart)Session["__MySessionCart"];

                    if (cart != null)
                    {
                        return Json(cart, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(cart, JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [Route("cart/checkout")]
        public ActionResult Checkout()
        {
            return View();
        }
    }
}