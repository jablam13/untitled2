﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Untitled2.Models.Calendar;
using Untitled2.Helpers;
using Untitled2.Repository;
using System.Security.Claims;

namespace Untitled2.Controllers
{
    public class CalendarController : Controller
    {
        private readonly CalendarRepository rep = new CalendarRepository();

        [Route("calendar")]
        public ActionResult Index()
        {
            var calendar = new Calendar();
            DateTime currentDate = DateTime.Now;


            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }

            var events = rep.GetEvents(id, currentDate.Month, currentDate.Year);

            var dates = events.Select(a => a.StartTime).ToList();

            List<Day> days = dates.Select(a => new Day()
            {
                Date = a.Date
            }).ToList();

            foreach (var day in days)
            {
                day.Events = events.Where(x => x.StartTime.Day == day.Date.Day
                    && x.StartTime.Month == day.Date.Month
                    && x.StartTime.Year == day.Date.Year).ToList();
            }

            calendar.Days = days;

            return View(calendar);
        }

        [Route("calendar/getmonth")]
        [HttpPost]
        public ActionResult GetMonth(string date)
        {
            var calendar = new Calendar();

            int id = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    id = Convert.ToInt32(userIdClaim.Value);
                }
            }
            var month = DateTime.Parse(date).Month;
            var year = DateTime.Parse(date).Year;

            var events = rep.GetEvents(id, month, year);

            var dates = events.Select(a => a.StartTime).ToList();

            List<Day> days = dates.Select(a => new Day()
            {
                Date = a.Date
            }).ToList();

            foreach (var day in days)
            {
                day.Events = events.Where(x => x.StartTime.Day == day.Date.Day
                    && x.StartTime.Month == day.Date.Month
                    && x.StartTime.Year == day.Date.Year).ToList();
            }

            calendar.Days = days;

            return Json(calendar, JsonRequestBehavior.AllowGet);
        }


        [Route("calendar/attend")]
        [HttpPost]
        public JsonResult Attend(int eventId, int isAttending)
        {
            int userId = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (userId == 0)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(rep.AttendEvents(userId, eventId, isAttending), JsonRequestBehavior.AllowGet);
            }
        }


        [Route("calendar/addevent")]
        [HttpPost]
        public JsonResult AddEvent(Event addEvent)
        {
            int userId = 0;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                // the principal identity is a claims identity.
                // now we need to find the NameIdentifier claim
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = Convert.ToInt32(userIdClaim.Value);
                }
            }

            if (userId == 0)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(rep.AddEvent(userId, addEvent), JsonRequestBehavior.AllowGet);
            }
        }

        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))  // Days: 1, 2 ... 31 etc.
                             .Select(day => new DateTime(year, month, day)) // Map each day to a date
                             .ToList(); // Load dates into a list
        }
    }
}