﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace Untitled2
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/modernizr/bundle").Include("~/Scripts/modernizr-{version}.js"));
            bundles.Add(new ScriptBundle("~/Scripts/third-party/bundle").IncludeDirectory("~/Scripts/third-party", "*.js", true));
            bundles.Add(new ScriptBundle("~/Scripts/AppScripts").IncludeDirectory("~/Scripts/app", "*.js", true));
            bundles.Add(new ScriptBundle("~/Scripts/hubs.js").Include("~/Scripts/hubs.js"));
            bundles.Add(new ScriptBundle("~/Scripts/asteroid.js").Include("~/Scripts/asteroid.js"));
            bundles.Add(new StyleBundle("~/Content/asteroid").Include("~/Content/site/asteroid.css"));
            bundles.Add(new StyleBundle("~/Content/site").Include("~/Content/site/Site.css"));
            bundles.Add(new StyleBundle("~/Content/Fullscreen").Include("~/Content/site/Fullscreen.css"));
            bundles.Add(new StyleBundle("~/Content/StoreLayout").Include("~/Content/site/StoreLayout.css"));
            bundles.Add(new StyleBundle("~/bundles/bootstrapcss").Include("~/Content/bootstrap/bootstrap.css"));
            bundles.Add(new StyleBundle("~/Content/jQueryUi").Include("~/Content/themes/base/*.css"));


            bundles.Add(new ScriptBundle("~/Scripts/jquery-2.2.3/bundle").Include("~/Scripts/third-party/jquery-2.2.0.js"));
            bundles.Add(new ScriptBundle("~/Scripts/moment/bundle").Include("~/Scripts/third-party/moment.js"));
            bundles.Add(new ScriptBundle("~/Scripts/bootstrap/bundle").Include("~/Scripts/third-party/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/Scripts/bootstrap-datetimepicker/bundle").Include("~/Scripts/third-party/bootstrap-datetimepicker.js"));
            bundles.Add(new StyleBundle("~/bundles/bootstrap-datetimepicker").Include("~/Content/bootstrap-datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/Scripts/LuceneSearch").Include(
                "~/Scripts/third-party/kickstart.js",
                "~/Scripts/third-party/prettify.js",
                "~/Scripts/third-party/jquery-2.2.0.js",
                "~/Scripts/third-party/modernizr-2.8.3.js",
                "~/Scripts/third-party/bootstrap.js"
                ));
            bundles.Add(new StyleBundle("~/Content/LuceneSearch").Include(
                "~/Content/style.css",
                "~/Content/kickstart/kickstart.css",
                "~/Content/style.css",
                "~/Content/style.lib.css",
                "~/Content/style.local.css",
                "~/Content/bootstrap/bootstrap.css"));

            //            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //"~/Scripts/third-party/bundled/jquery-2.1.1.js"));

            //bundles.Add(new LessBundle("~/content/styles/third-party/bundle").IncludeDirectory("~/content/styles/third-party/", true));

            //bundles.Add(new LessBundle("~/content/styles/themes/bjc/bundle").Include("~/content/styles/themes/bjc/bjc.less"));
        }
    }
}