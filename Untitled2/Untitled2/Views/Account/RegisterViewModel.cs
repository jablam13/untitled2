﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Untitled2.Helpers;
using Untitled2.Models;

namespace Untitled2.Views.Account
{
    public class RegisterViewModel
    {
        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [PasswordComplexity(MinLength = 6, RequiredUppercaseCharacters = 1, RequiredLowercaseCharacters = 1, RequiredNumberCharacters = 1, RequiredSpecialCharacters = 1)]
        public string Password { get; set; }

        [Required]
        [MustEqualProperty(OtherPropertyName = "Password")]
        public string PasswordRpt { get; set; }

        public int? Status { get; set; }

        public List<string> Errors { get; set; }
    }
}