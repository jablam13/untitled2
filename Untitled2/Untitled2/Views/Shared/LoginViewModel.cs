﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Untitled2.Helpers;
using Untitled2.Models;

namespace Untitled2.Views.Account
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string ErrorMessage { get; set; }
        public bool RememberMe { get; set; }
        public Dictionary<string, List<string>> FormFieldErrors { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int? Status { get; set; }
        public string StatusMessage { get; set; }
        public List<string> Errors { get; set; }
        public Dictionary<string, List<string>> FormFieldErrors { get; set; }
    }
}