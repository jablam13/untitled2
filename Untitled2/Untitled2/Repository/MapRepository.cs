﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Models.Map;

namespace Untitled2.Repository
{
    public class MapRepository : RepositoryBase
    {
        public List<Places> GetLists(int? userId)
        {
            var sqlList = @"SELECT p.Id as Id, p.Uid as Uid, P.Name as Name,
p.description as Description
FROM PlaceList p JOIN Registrant r ON r.Id = p.UserId
WHERE UserId = @UserId AND p.active = 1; ";
            var sqlListItem = @"select p.Id, p.Uid, p.ListId, p.Lat, p.Lng, p.Name, p.Description, p.GooglePlaceId,
g.icon, g.name as GoogleName, g.price_level, g.rating, g.vicinity, g.formatted_address,
g.international_phone_number, g.website, p.active
 from placeitem p join GooglePlace g on g.placeId = p.GooglePlaceId 
 where p.listid = @ListId and active = 1 ";

            var placesList = Query<Places>(sqlList, new { UserId = userId }).ToList();

            foreach (var list in placesList)
            {
                var places = Query<Place>(sqlListItem, new { ListId = list.Id }).ToList();
                list.PlaceList = places;
            }

            return placesList;
        }

        public Places AddList(Places list, int userId)
        {
            var sql = @"INSERT INTO PlaceList
SELECT @UserId, NEWID(), NULL, @Name, @Description, 1, GETDATE(), GETDATE();

SELECT p.Id as Id, p.Uid as Uid, p.Name as Name,
p.description as Description
FROM PlaceList p join Registrant r on r.Id = p.UserId
WHERE p.Id = SCOPE_IDENTITY(); 
";
            return Query<Places>(sql,
                new
                {
                    Name = list.Name,
                    Description = list.Description,
                    UserId = userId
                }).FirstOrDefault();

        }
        public void RemoveList(int listId, int userId)
        {
            var sql = @"UPDATE PlaceList SET Active = 0 WHERE Id = @ListId and UserId = @UserId;";
            Execute(sql,
                new
                {
                    ListId = listId,
                    UserId = userId
                });
        }

        /*update for specific user*/
        public Place RemovePlace(int placeId, string googlePlaceId)
        {
            var sql = @"UPDATE PlaceItem SET Active = 0 WHERE Id = @PlaceId and GooglePlaceId = @GooglePlaceId;

SELECT p.Id, p.Uid, p.ListId, p.Lat, p.Lng, p.Name, p.Description, p.GooglePlaceId,
g.icon, g.name as GoogleName, g.price_level, g.rating, g.vicinity, g.formatted_address,
g.international_phone_number, g.website, p.active
FROM PlaceItem p JOIN GooglePlace g on g.PlaceId = p.GooglePlaceId 
WHERE p.Id = @PlaceId AND Active = 0 AND g.PlaceId = @GooglePlaceId;
";
            return Query<Place>(sql,
                new
                {
                    PlaceId = placeId,
                    GooglePlaceId = googlePlaceId
                }).FirstOrDefault();
        }

        public Place AddItem(Place list, int userId)
        {
            var sql = @"
IF NOT EXISTS (SELECT * From GooglePlace WHERE placeId = @GooglePlaceId)
BEGIN 
	INSERT INTO GooglePlace 
	SELECT @GooglePlaceId, @icon,@GoogleName,  @price_level,@rating, @vicinity, 
		@formatted_address, NULL, @international_phone_number, @permanently_closed, @website, GETDATE(), GETDATE()
END

IF NOT EXISTS (SELECT * FROM PlaceItem pli join PlaceList pl on pli.ListId = pl.id where UserId = @UserId and pl.Id = @ListId and pli.GooglePlaceId = @GooglePlaceId)
BEGIN
    INSERT INTO PlaceItem
	SELECT NEWID(), @ListId, @Lat, @Lng, @Name, @Description, @GooglePlaceId, 1, getdate(), getdate()
END
ELSE
	IF (SELECT pli.Active FROM PlaceItem pli join PlaceList pl on pli.ListId = pl.id where UserId = @UserId and pli.GooglePlaceId = @GooglePlaceId and pl.Id = @ListId ) = 1
		update PlaceItem set Active = 1, LastModifiedDate = GETDATE()  where ListId = @ListId and GooglePlaceId = @GooglePlaceId
	ELSE 
		update PlaceItem set Active = 1, LastModifiedDate = GETDATE() where ListId = @ListId and GooglePlaceId = @GooglePlaceId;

select p.Id, p.Uid, p.ListId, l.uid as ListUid, p.Lat, p.Lng, p.Name, p.Description, p.GooglePlaceId,
g.icon, g.name as GoogleName, g.price_level, g.rating, g.vicinity, g.formatted_address,
g.international_phone_number, g.website, p.active
from placeitem p 
join GooglePlace g on g.placeId = p.GooglePlaceId 
join placelist l on l.id = p.ListId
where p.listid = @ListId and p.active = 1 AND p.GooglePlaceId = @GooglePlaceId;
";

            return Query<Place>(sql, new
            {
                UserId = userId,
                ListId = list.ListId,
                Lat = list.Lat,
                Lng = list.Lng,
                Name = list.Name,
                Description = list.Description,
                GooglePlaceId = list.GooglePlaceId,
                icon = list.icon,
                GoogleName = list.GoogleName,
                price_level = list.price_level,
                rating = list.rating,
                vicinity = list.vicinity,
                formatted_address = list.formatted_address,
                international_phone_number = list.international_phone_number,
                permanently_closed = list.permanently_closed,
                website = list.website
            }).FirstOrDefault();
        }
    }
}