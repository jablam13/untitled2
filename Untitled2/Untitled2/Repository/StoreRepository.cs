﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Models;
using Untitled2.Helpers;

namespace Untitled2.Repository
{
    public class StoreRepository : RepositoryBase
    {
        private readonly static int siteId = 5;

        public List<Store> GetStores(int userId)
        {
            var sql = @"SELECT s.Id, s.[Uid], r.Id as CreatorId, r.FirstName + ' ' + SUBSTRING(r.LastName,0,2) + '.' as CreatorName, 
s.Name, s.[Description], s.LargeImg, s.SmallImg, s.Active, s.CreatedDate, s.LastModifiedDate
 FROM Store s LEFT JOIN Registrant r on r.Id = s.CreatorId;";

            var stores = Query<Store>(sql, new { Id = userId }).ToList();

            return stores;
        }
        public Store GetStoreAll(int userId, Guid storeGuid)
        {
            var sql = @"SELECT s.Id, s.[Uid], r.Id as CreatorId, r.FirstName + ' ' + SUBSTRING(r.LastName,0,2) + '.' as CreatorName, 
s.Name, s.[Description], s.LargeImg, s.SmallImg, s.Active, s.CreatedDate, s.LastModifiedDate
 FROM Store s LEFT JOIN Registrant r on r.Id = s.CreatorId
WHERE s.[Uid] = @StoreGuid;";
            var store = Query<Store>(sql, new { Id = userId, StoreGuid = storeGuid }).FirstOrDefault();

            store.Categories = GetCategories(store.Id);

            foreach (var category in store.Categories)
            {
                category.StoreItems = GetStoreItems(userId, store.Id, category.Id);
            }

            return store;
        }

        public Store GetStore(int userId)
        {
            var sql = @"SELECT s.Id, s.[Uid], r.Id as CreatorId, r.FirstName + ' ' + SUBSTRING(r.LastName,0,2) + '.' as CreatorName, 
s.Name, s.[Description], s.LargeImg, s.SmallImg, s.Active, s.CreatedDate, s.LastModifiedDate
 FROM Store s LEFT JOIN Registrant r on r.Id = s.CreatorId;";
            var store = Query<Store>(sql, new { Id = userId }).FirstOrDefault();


            return store;
        }

        public List<StoreCategory> GetCategories(int storeId)
        {
            var sql = @"SELECT c.Id, c.Uid, c.StoreId, c.Name,c.Description,c.LargeImg, c.SmallImg, c.Active, c.CreatedDate, c.LastModifiedDate FROM Category c
WHERE c.StoreId = @StoreId;";
            var categories = Query<StoreCategory>(sql, new { StoreId = storeId }).ToList();

            return categories;
        }
        public List<StoreItem> GetStoreItems(int userId, int storeId, int? categoryId)
        {
            var sql = @"
WITH CartItems AS 
(
	SELECT ci.Id, ci.ItemId, ci.Quantity, c.UserId FROM CartItem ci 
	LEFT JOIN Cart c on c.Id = ci.CartId 
    LEFT JOIN UserDetails u on u.Id = c.UserId 
	WHERE c.UserId = @UserId AND u.SiteId = @SiteId
)
SELECT sii.Id, sii.[Uid], sii.StoreId, si.Id as StoreItemId, si.[Uid] StoreItemUid, 
ISNULL(sii.CategoryId, -1) as CategoryId, si.Name, si.[Description], sii.Price, 
sii.Quantity - ISNULL(ci.Quantity, 0) as Quantity,
si.Active as [ItemActive], sii.Active as [StoreItemActive], sii.Quantity as TotalQuantity
 FROM StoreItem si 
LEFT JOIN StoreItemInfo sii on sii.StoreItemId = si.Id 
LEFT JOIN CartItems ci on ci.ItemId = sii.Id
WHERE sii.StoreId = @StoreId
AND sii.CategoryId = @CategoryId;";
            var user = Query<StoreItem>(sql, new { UserId = userId, StoreId = storeId, CategoryId = categoryId, SiteId = siteId }).ToList();

            return user;
        }
        public StoreItem GetStoreItem(int userId, Guid storeItemGuid)
        {
            var sql = @"
WITH CartItems AS 
(
	SELECT ci.Id, ci.ItemId, ci.Quantity, c.UserId 
    FROM CartItem ci 
	LEFT JOIN Cart c on c.Id = ci.CartId 
    LEFT JOIN UserDetails u on u.Id = c.UserId 
	WHERE c.UserId = @UserId AND u.SiteId = @SiteId
)
SELECT sii.Id, sii.[Uid], sii.StoreId, si.Id as StoreItemId, si.[Uid] StoreItemUid, 
ISNULL(sii.CategoryId, -1) as CategoryId, si.Name, si.[Description], sii.Price, 
sii.Quantity - ISNULL(ci.Quantity, 0) as Quantity, 
si.Active as [ItemActive], sii.Active as [StoreItemActive], sii.Quantity as TotalQuantity
 FROM StoreItem si 
LEFT JOIN StoreItemInfo sii on sii.StoreItemId = si.Id 
LEFT JOIN CartItems ci on ci.ItemId = sii.Id
WHERE sii.Uid = @StoreItemGuid;";
            var storeItem = Query<StoreItem>(sql, new { UserId = userId, StoreItemGuid = storeItemGuid, SiteId = siteId }).FirstOrDefault();

            return storeItem;
        }

        public Cart CartTransaction(int userId, StoreItem item)
        {
            var sql = @"
DECLARE @OutputCart TABLE(Id INT);
DECLARE @OutputCartItem TABLE(Id INT, Quantity INT);
DECLARE @StoreQuantity INT = (SELECT Quantity FROM StoreItemInfo WHERE Id = @ItemId),
		@UserQuantity INT = (SELECT Quantity FROM CartItem ci JOIN Cart c on c.Id = ci.CartId WHERE ci.ItemId = @ItemId and c.UserId = @UserId);

--Create cart if one hasnt been created.
IF NOT EXISTS(SELECT * FROM Cart WHERE UserId = @UserId)
BEGIN
	INSERT INTO Cart
	OUTPUT INSERTED.Id INTO @OutputCart(Id)
	SELECT TOP 1 NEWID(), @UserId, GETDATE(),GETDATE()
END
ELSE 
	INSERT INTO @OutputCart
	SELECT Id FROM Cart WHERE UserId = @UserId
	
IF(@CartTransaction = 'ADD')
BEGIN
	--if the item already exists, update the quantity
	IF EXISTS(SELECT * FROM CartItem WHERE CartId = (SELECT Id FROM @OutputCart) and ItemId = @ItemId)
	BEGIN
		UPDATE CartItem SET Quantity = (CASE
			WHEN @Quantity + @UserQuantity > @StoreQuantity THEN @StoreQuantity
			ELSE @Quantity + @UserQuantity
		END) 
		OUTPUT INSERTED.Id, INSERTED.Quantity INTO @OutputCartItem(Id, Quantity)
		WHERE CartId = (SELECT TOP 1 Id FROM @OutputCart) AND ItemId = @ItemId  
	END
	--insert the item into the cart
	ELSE
	BEGIN
		INSERT INTO CartItem
		OUTPUT INSERTED.Id, INSERTED.Quantity INTO @OutputCartItem(Id, Quantity)
		SELECT TOP 1 NEWID(), (SELECT Id FROM @OutputCart), @ItemId, @Quantity, GETDATE(), GETDATE();
	END 

	--log the transaction
	INSERT INTO CartLog
	SELECT TOP 1 NEWID(), (SELECT Id FROM @OutputCartItem), @Quantity, @Price, 1, GETDATE();
END
ELSE IF (@CartTransaction = 'REMOVE')
BEGIN
	DELETE FROM CartItem
	OUTPUT DELETED.Id, DELETED.Quantity INTO @OutputCartItem(Id, Quantity)
	WHERE CartId = (SELECT Id FROM Cart WHERE UserId = @UserId) AND ItemId = @ItemId

	INSERT INTO CartLog
	SELECT TOP 1 NEWID(), (SELECT Id FROM @OutputCartItem), - @Quantity, @Price, 2, GETDATE();
END
ELSE IF (@CartTransaction = 'UPDATE')
BEGIN
    UPDATE CartItem SET Quantity = CASE
		WHEN @Quantity < 0 THEN 0 
		WHEN @Quantity > @StoreQuantity THEN @StoreQuantity
		ELSE @Quantity 
		END, LastModifiedDate = GETDATE()
	OUTPUT INSERTED.Id, DELETED.Quantity INTO @OutputCartItem
	WHERE CartId = (SELECT TOP 1 Id FROM Cart WHERE UserId = @UserId) 
	AND ItemId = @ItemId; 

	INSERT INTO CartLog
	SELECT TOP 1 NEWID(), (SELECT Id FROM @OutputCartItem),(SELECT @Quantity - Quantity FROM @OutputCartItem), @Price, 3, GETDATE();
END
ELSE 
    SELECT -1 as Status;


IF(@CartTransaction = 'ADD')
BEGIN
	SELECT sii.Id, sii.[Uid], sii.StoreId, si.Id as StoreItemId, si.[Uid] StoreItemUid, 
	ISNULL(sii.CategoryId, -1) as CategoryId, si.Name, si.[Description], sii.Price, 
	sii.Quantity - ISNULL(ci.Quantity, 0) as Quantity, 
	si.Active as [ItemActive], sii.Active as [StoreItemActive], sii.Quantity as TotalQuantity
	FROM StoreItem si 
	LEFT JOIN StoreItemInfo sii on sii.StoreItemId = si.Id 
	LEFT JOIN CartItem ci on ci.ItemId = sii.Id
	LEFT JOIN Cart c on c.Id = ci.CartId 
	WHERE sii.[Uid] = @StoreItemGuid
	AND UserId = @UserId;
END
ELSE IF(@CartTransaction = 'UPDATE' OR @CartTransaction = 'REMOVE')
BEGIN
	SELECT sii.Id, sii.[Uid], si.Id as StoreItemId, 
	si.[Uid] as StoreItemUid, sii.StoreId, sii.CategoryId, si.Name, 
	si.[Description], sii.Price, ISNULL(SUM(ci.Quantity),0) as Quantity,
	si.LargeImg, si.SmallImg, sii.Active, sii.Quantity as TotalQuantity
	FROM Cart c LEFT JOIN CartItem ci on ci.CartId = c.Id
	LEFT JOIN StoreItemInfo sii on sii.Id = ci.ItemId
	LEFT JOIN StoreItem si on sii.StoreItemId = si.Id
	WHERE UserId = @UserId 
	AND sii.[Uid] = @StoreItemGuid
	GROUP BY sii.Id, sii.[Uid], si.Id, 
	si.[Uid], sii.StoreId, sii.CategoryId, si.Name, 
	si.[Description], sii.Price, sii.Quantity, si.LargeImg, si.SmallImg, sii.Active;
END;";

            var updatedStoreItems = Query<StoreItem>(sql, new
            {
                UserId = userId,
                CartTransaction = item.CartTransaction,
                ItemId = item.Id,
                StoreItemGuid = item.Uid,
                Quantity = item.Quantity,
                Price = item.Price
            }).ToList();

            var updatedCart = GetCart(userId);

            updatedCart.CartItems = updatedStoreItems;

            return updatedCart;
        }
        public Cart GetCartAndItems(int userId)
        {
            var cart = GetCart(userId);

            cart.CartItems = CartItems(userId);

            return cart;
        }

        public Cart GetCart(int userId)
        {
            if (userId == 0)
            {
                return new Cart();
            }
            else
            {
                var sql = @"
WITH CartModel (Id, Uid, Price, Quantity) AS (
	SELECT c.Id, c.[Uid],
	sii.Price, ci.Quantity as Quantity
	FROM Cart c LEFT JOIN CartItem ci on ci.CartId = c.Id
	LEFT JOIN StoreItemInfo sii on sii.Id = ci.ItemId
	LEFT JOIN StoreItem si on sii.StoreItemId = si.Id
	WHERE UserId = @UserId
	GROUP BY c.Id, c.[Uid], si.Id, sii.Price, ci.Quantity
)
SELECT Id, Uid, SUM(Price * Quantity) as TotalPrice, SUM(Quantity) as Quantity FROM CartModel
GROUP BY  Id, Uid;";

                var cart = Query<Cart>(sql, new { UserId = userId }).FirstOrDefault();

                if (cart == null) { cart = new Cart(); }

                return cart;
            }
        }
        public List<StoreItem> CartItems(int userId)
        {
            if (userId == 0)
            {
                return new List<StoreItem>();
            }
            else
            {
                var sql = @"
SELECT sii.Id, sii.[Uid], si.Id as StoreItemId, 
si.[Uid] as StoreItemUid, sii.StoreId, sii.CategoryId, si.Name, 
si.[Description], sii.Price, ISNULL(SUM(ci.Quantity),0) as Quantity,
si.LargeImg, si.SmallImg, sii.Active, sii.Quantity as TotalQuantity
FROM Cart c LEFT JOIN CartItem ci on ci.CartId = c.Id
LEFT JOIN StoreItemInfo sii on sii.Id = ci.ItemId
LEFT JOIN StoreItem si on sii.StoreItemId = si.Id
WHERE UserId = @UserId 
AND ci.Quantity > 0
GROUP BY sii.Id, sii.[Uid], si.Id, 
si.[Uid], sii.StoreId, sii.CategoryId, si.Name, 
si.[Description], sii.Price, sii.Quantity, si.LargeImg, si.SmallImg, sii.Active;
";

                var cartItems = Query<StoreItem>(sql, new
                {
                    UserId = userId
                }).ToList();

                if (cartItems == null) { cartItems = new List<StoreItem>(); }

                return cartItems;
            }
        }
    }

}