﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Models;

namespace Untitled2.Repository
{
    public class GameRepository : RepositoryBase
    {
        //Get League
        public GameModel GetGameModel(string email)
        {
            //needs Value for PlayerId or Username/UserId


            var sqlSports = @"SELECT Id, Name FROM Sport;";

            //var sqlType = @"SELECT * FROM LeagueType";

            var games = new GameModel();
            games.Games = GetGames(email);
            games.Sports = Query<Sport>(sqlSports).ToList();
            //foreach (var l in leagues)
            //{
            //    l.Type = Query<LeagueType>(sqlType, new { LeagueId = l.Id }).FirstOrDefault();
            //}

            return games;
        }

        public List<Game> GetGames(string email)
        {
            var sqlGames = @"
DECLARE @PlayerId INT = (SELECT TOP 1 Id FROM Player WHERE Username = @Email);

SELECT g.Id, 
	g.Name, s.Id as SportId, s.Name as Sport, g.StartTime, g.HomeTeamId, t1.Name AS HomeTeamName,
	g.AwayTeamId, t2.Name AS AwayTeamName,
	g.HomeScore, g.AwayScore, g.[Public], g.Active,
	(SELECT CASE WHEN EXISTS(SELECT * FROM TeamPlayer WHERE PlayerId = @PlayerId AND (TeamId = t1.Id or TeamId = t2.Id)) THEN 1 ELSE 0 END) AS CurrentlyPlaying,
    (SELECT COUNT(*) FROM TeamPlayer WHERE TeamId in (g.HomeTeamId, g.AwayTeamId)) as [Count]
FROM Game g
LEFT JOIN Team t1 on t1.Id = g.HomeTeamId
LEFT JOIN Team t2 on t2.Id = g.AwayTeamId
LEFT JOIN Sport s on s.Id = g.SportId 
ORDER BY g.StartTime DESC;";

            return Query<Game>(sqlGames, new { Email = email }).ToList();
        }

        public Game AddGame(Player player, Game game)
        {
            var sqlAddGame = @"

DECLARE @PlayerId TABLE(Id INT);
DECLARE @TeamId TABLE(Id INT);
DECLARE @GameId TABLE(Id INT);

IF NOT EXISTS(SELECT * FROM Player WHERE Username = @Email)
	BEGIN 
		INSERT INTO Player 
		OUTPUT INSERTED.ID INTO @PlayerId(Id)
		SELECT @Email, @FirstName, @LastName, 1, GETDATE(), GETDATE()
	END
ELSE 
	INSERT INTO @PlayerId 
	SELECT TOP 1 Id FROM Player WHERE Username = @Email;

INSERT INTO Team
OUTPUT INSERTED.ID INTO @TeamId(Id)
SELECT 'Team A', Id, Id, 1, GETDATE(), GETDATE() FROM @PlayerId
UNION 
SELECT 'Team B', Id, Id, 1, GETDATE(), GETDATE() FROM @PlayerId

INSERT INTO Game 
OUTPUT INSERTED.ID INTO @GameId(Id)
SELECT @GameName , @SportId, NULL, @StartDate, 
	(SELECT TOP 1 Id FROM @TeamId),
	(SELECT TOP 1 Id FROM @TeamId ORDER BY Id DESC ), 
	NULL, NULL, @Public, 1, GETDATE(), GETDATE() 

INSERT INTO TeamPlayer
SELECT Id, (SELECT TOP 1 Id FROM @TeamId), GETDATE(), GETDATE() FROM @PlayerId;

SELECT g.Id, 
	g.Name, s.Id as SportId, s.Name as Sport, g.StartTime, g.HomeTeamId, t1.Name AS HomeTeamName,
	g.AwayTeamId, t2.Name AS AwayTeamName,
	g.HomeScore, g.AwayScore, g.[Public], g.Active,
	0 AS CurrentlyPlaying,
    (SELECT COUNT(*) FROM TeamPlayer WHERE TeamId in (g.HomeTeamId, g.AwayTeamId)) as [Count]
FROM Game g
LEFT JOIN Team t1 on t1.Id = g.HomeTeamId
LEFT JOIN Team t2 on t2.Id = g.AwayTeamId
LEFT JOIN Sport s on s.Id = g.SportId
WHERE g.Id = (SELECT TOP 1 Id FROM @GameId);
";
            return Query<Game>(sqlAddGame, new
            {
                FirstName = player.FirstName,
                LastName = player.LastName,
                GameName = game.Name,
                SportId = game.SportId,
                Email = player.Username,
                Public = game.Public,
                StartDate = game.StartTime
            }).FirstOrDefault();
        }

        public Game JoinLeaveGame(Player player, int gameId)
        {
            var sql = @"
DECLARE @PlayerId TABLE(Id INT);
DECLARE @HomeCount INT,
		@AwayCount INT;


--check and see if player exists.
--if doesnt exists, add player and save PlayerId variable
IF NOT EXISTS(SELECT * FROM Player WHERE Username = @Email)
	BEGIN 
		INSERT INTO Player 
		OUTPUT INSERTED.ID INTO @PlayerId(Id)
		SELECT @Email, @FirstName, @LastName, 1, GETDATE(), GETDATE()
	END
ELSE 
--if exists, find player, save playerId variable
	INSERT INTO @PlayerId 
	SELECT TOP 1 Id FROM Player WHERE Username = @Email;

--Count Players On Home Team
SELECT @HomeCount = COUNT(hp.PlayerId)FROM Game g
LEFT JOIN TeamPlayer hp  on g.HomeTeamId = hp.TeamId 
LEFT JOIN @PlayerId p on p.Id = hp.PlayerId 
WHERE g.Id = @GameId

--Count Players On Away Team
SELECT @AwayCount = COUNT(ap.PlayerId) FROM Game g
LEFT JOIN TeamPlayer ap  on g.AwayTeamId = ap.TeamId
LEFT JOIN @PlayerId p on p.Id = ap.PlayerId 
WHERE g.Id = @GameId


--check and see if player is exists in game
IF EXISTS(SELECT * FROM TeamPlayer tp 
JOIN @PlayerId p on p.Id = tp.PlayerId 
JOIN Game g on g.HomeTeamId = tp.TeamId OR g.AwayTeamId = tp.TeamId
WHERE g.Id = @GameId)
	BEGIN
		--If Exists, Remove From Game
		DELETE tp FROM TeamPlayer tp 
		JOIN @PlayerId p on p.Id = tp.PlayerId 
		JOIN Game g on g.HomeTeamId = tp.TeamId OR g.AwayTeamId = tp.TeamId
		WHERE g.Id = @GameId
	END
--Else, Add To Game
ELSE
	BEGIN
		--If Home Team Has More People, Adds to Away
		IF(@HomeCount > @AwayCount)
			BEGIN
				INSERT INTO TeamPlayer
				SELECT (SELECT TOP 1 Id FROM @PlayerId), AwayTeamId,GETDATE(),GETDATE() FROM Game WHERE Id = @GameId 
			END
		--If Home Team Has Less or the Same Amount People, Adds to Home
		ELSE IF(@HomeCount <= @AwayCount)
			BEGIN
				INSERT INTO TeamPlayer
				SELECT (SELECT TOP 1 Id FROM @PlayerId), HomeTeamId,GETDATE(),GETDATE() FROM Game WHERE Id = @GameId 
			END
	END;

SELECT g.Id, 
	g.Name, s.Id as SportId, s.Name as Sport, g.StartTime, g.HomeTeamId, t1.Name AS HomeTeamName,
	g.AwayTeamId, t2.Name AS AwayTeamName,
	g.HomeScore, g.AwayScore, g.[Public], g.Active,
	(SELECT CASE WHEN EXISTS(SELECT * 
	 FROM TeamPlayer 
	 WHERE PlayerId = (SELECT TOP 1 Id FROM @PlayerId) 
	 AND (TeamId = t1.Id or TeamId = t2.Id)) THEN 1 ELSE 0 END) AS CurrentlyPlaying,
    (SELECT COUNT(*) FROM TeamPlayer WHERE TeamId in (g.HomeTeamId, g.AwayTeamId)) as [Count]
FROM Game g
LEFT JOIN Team t1 on t1.Id = g.HomeTeamId
LEFT JOIN Team t2 on t2.Id = g.AwayTeamId
LEFT JOIN Sport s on s.Id = g.SportId
WHERE g.Id = @GameId;";
            return Query<Game>(sql, new
            {
                FirstName = player.FirstName,
                LastName = player.LastName,
                GameId = gameId,
                Email = player.Username
            }).FirstOrDefault();
        }
    }
}