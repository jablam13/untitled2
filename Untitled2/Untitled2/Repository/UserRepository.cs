﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Untitled2.Models;
using Untitled2.Helpers;

namespace Untitled2.Repository
{
    public class UserRepository : RepositoryBase
    {
        private readonly static int siteId = 5;
        public User Get(int userId)
        {
            var user = Query<User>(@"SELECT r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
	u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
 FROM Registrant r left join UserDetails u ON u.Registrantid = r.Id 
 WHERE r.Id = @Id and u.SiteId = @SiteId;", new { Id = userId, SiteId = siteId }).FirstOrDefault();

            return user;
        }
        public User Get(string email)
        {
            var user = Query<User>(@"SELECT r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
 u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
 FROM Registrant r left join UserDetails u ON u.Registrantid = r.Id 
 WHERE r.EmailAddress = @EmailAddress and u.SiteId = @SiteId;", new { EmailAddress = email, SiteId = siteId }).FirstOrDefault();

            return user;
        }
        public User GetNewValidator(string email)
        {
            var user = Query<User>(@"
DECLARE @validator UNIQUEIDENTIFIER;

UPDATE UserDetails SET Validator = NEWID(), LastModifiedDate = GETDATE() WHERE RegistrantId = (SELECT Id FROM Registrant WHERE EmailAddress = @EmailAddress) AND SiteId = @SiteId;

SELECT r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
 u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
 FROM Registrant r left join UserDetails u ON u.Registrantid = r.Id 
 WHERE r.EmailAddress = @EmailAddress and u.SiteId = @SiteId", new { EmailAddress = email, SiteId = siteId }).FirstOrDefault();

            return user;
        }

        public User Register(User user, string password)
        {
            var salt = PasswordUtilities.GenerateSalt();
            var hash = PasswordUtilities.GenerateHash(password, salt);

            var sql = @"
DECLARE @OutputTable TABLE(Id INT);

IF(NOT EXISTS( SELECT * FROM Registrant r WHERE r.EmailAddress = 'jablam@hotmail.com'))
BEGIN
	INSERT INTO Registrant(Uid, FirstName, LastName, Age, Gender, EmailAddress, PhoneNumber, Description, CreatedDate, LastModifiedDate)
	OUTPUT INSERTED.ID INTO @OutputTable(Id)
	VALUES (NEWID(), @FirstName, @LastName, @Age, @Gender, @EmailAddress, @PhoneNumber, @Description, GETDATE(),GETDATE());
END
ELSE 
BEGIN
	INSERT INTO @OutputTable 
	SELECT Id FROM Registrant WHERE EmailAddress = 'jablam@hotmail.com' 
END 
IF(NOT EXISTS( SELECT * FROM Registrant r JOIN UserDetails u on u.RegistrantId = r.Id WHERE r.EmailAddress = @EmailAddress AND u.SiteId = @SiteId))
BEGIN
	INSERT INTO UserDetails(RegistrantId, AdminPending, IsAdmin, LeagueAdmin, TeamAdmin, Hash, Salt, OpenId, OpenIdClaim, Validator, InActive, CreatedDate, LastModifiedDate, SiteId, Uid)
	VALUES ((SELECT Id FROM @OutputTable), @AdminPending, @IsAdmin, @LeagueAdmin, @TeamAdmin, @Hash, @Salt, @OpenId, @OpenIdClaim, NEWID(), 1, GETDATE(),GETDATE(), @SiteId, NEWID());

	SELECT TOP 1 r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
	 u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
	 FROM Registrant r 
	 LEFT JOIN UserDetails u ON u.RegistrantId = r.Id 
	 JOIN @OutputTable o on o.Id = r.Id
     WHERE u.SiteId = @SiteId;
END 
ELSE 
	SELECT TOP 1 r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
	 u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
	 FROM Registrant r 
	 LEFT JOIN UserDetails u ON u.RegistrantId = r.Id 
	 WHERE r.EmailAddress = @EmailAddress AND u.SiteId = @SiteId;
";
            var newUser = Query<User>(sql,
                new
                {
                    EmailAddress = user.EmailAddress,
                    Hash = hash,
                    Salt = salt,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Age = user.Age,
                    Gender = "Men" /*user.Gender*/,
                    PhoneNumber = user.PhoneNumber,
                    Description = user.Description,
                    AdminPending = 0,
                    IsAdmin = 0,
                    LeagueAdmin = 0,
                    TeamAdmin = user.TeamAdmin,
                    OpenId = user.OpenId,
                    OpenIdClaim = user.OpenIdClaim,
                    Validator = user.Validator,
                    InActive = user.InActive,
                    SiteId = siteId
                }).FirstOrDefault();

            return newUser;
        }

        public bool IsUsernameAvailable(string email)
        {
            var sql = @"
SELECT COUNT(u.Id)
FROM Registrant r
JOIN UserDetails u on u.RegistrantId = r.Id 
WHERE r.EmailAddress = @EmailAddress AND u.SiteId = @SiteId 
";

            var isUsernameAvailable = Query<int>(sql, new { EmailAddress = email, SiteId = siteId }).FirstOrDefault() == 0;
            return isUsernameAvailable;
        }

        public bool IsUserExists(string email)
        {
            var sql = @"
SELECT CAST(COUNT(u.Id) AS BIT) 
FROM Registrant r
JOIN UserDetails u on u.RegistrantId = r.Id 
WHERE r.EmailAddress = @EmailAddress AND u.SiteId = @SiteId 
";

            var isUsernameExists = Query<int>(sql, new { EmailAddress = email, SiteId = siteId }).FirstOrDefault() == 1;
            return isUsernameExists;
        }

        public User ValidateEmail(Guid? guid)
        {
            User user = null;
            var sql = @"
DECLARE @Email NVARCHAR(100);

SET @Email = (SELECT TOP 1 r.EmailAddress FROM Registrant r JOIN UserDetails u on u.RegistrantId = r.Id WHERE u.Validator = @Validator AND u.SiteId = @SiteId);

IF EXISTS(SELECT * FROM UserDetails WHERE Validator = @Validator AND SiteId = @SiteId)
BEGIN
	UPDATE UserDetails SET InActive = 0, Validator = NULL WHERE Validator = @Validator AND SiteId = @SiteId
	SELECT TOP 1 r.Id, r.FirstName, r.LastName, r.EmailAddress, r.EmailAddress as Username,
	 u.Id as UserId, u.Uid as UserGuid, u.IsAdmin, u.LeagueAdmin, u.TeamAdmin, u.OpenId, u.OpenIdClaim, u.Validator, u.InActive
	 FROM Registrant r 
	 LEFT JOIN UserDetails u ON u.RegistrantId = r.Id 
	 WHERE r.EmailAddress = @Email AND u.SiteId = @SiteId;
END;
";
            user = Query<User>(sql, new { Validator = guid, SiteId = siteId }).FirstOrDefault();
            return user;
        }

        public AuthLoginAttempt GetParticipantByUsername(string email)
        {
            var sql = @"
SELECT r.Id, u.Id as UserId, u.Uid as UserGuid,  u.Hash, u.Salt, PaxUID = r.uid
FROM Registrant r 
LEFT JOIN UserDetails u ON u.Registrantid = r.Id 
WHERE r.EmailAddress = @EmailAddress AND u.SiteId = @SiteId;
";
            var authLoginAttempt = Query<AuthLoginAttempt>(sql, new { EmailAddress = email, SiteId = siteId }).SingleOrDefault();
            return authLoginAttempt;
        }

        public bool LogSuccessfulLogin(string email)
        {
            var sql = @"
DECLARE @Id INT;
SELECT @Id = Id from Users where Username = @Username;
IF @Id IS NOT NULL
BEGIN
    UPDATE Users SET LoginAttempts = 0, LastLoginDate = GETDATE(), LastModifiedDate = GETDATE() WHERE Id = @Id;
    Insert Into Users2Activities (UserId, ActivityTypeId, CreatedDate, LastModifiedDate) Values (@id, @ActivityTypeId,GetDate(), GetDate());
	SELECT 1
END
ELSE
BEGIN
	SELECT 0
END
";
            var success = Query<bool>(sql, new { EmailAddress = email }).SingleOrDefault();
            return success;
        }

        public bool LogFailedLoginAttempt(string username)
        {
            var sql = @"
IF (SELECT ISNULL(LoginAttempts, 0) FROM Users WHERE Username = @Username) >= 10
BEGIN
    UPDATE Users SET LoginAttempts = LoginAttempts+1, DeletionDate = GETDATE(), LastModifiedDate = GETDATE() WHERE Username = @Username;
	SELECT 1
END
ELSE
BEGIN
    UPDATE Users SET LoginAttempts = LoginAttempts+1, LastModifiedDate = GETDATE() WHERE Username = @Username;
	SELECT 0
END
";
            var failed = Query<bool>(sql, new { Username = username }).SingleOrDefault();
            return failed;
        }
        public ForgotPassword IsUsernameAvailable(Guid validator)
        {
            var sql = @"
IF(EXISTS(SELECT * FROM Registrant r JOIN UserDetails u ON r.Id = u.RegistrantId WHERE u.Validator = @Validator))
BEGIN
    SELECT TOP 1 r.Id, r.Uid, r.EmailAddress as Email, 1 AS Status, u.Id as UserId, u.Uid as UserGuid FROM Registrant r 
    JOIN UserDetails u ON r.Id = u.RegistrantId
    WHERE u.Validator = @Validator AND u.SiteId = @SiteId;
END
ELSE
    SELECT 0 AS Status;
";

            var isUsernameAvailable = Query<ForgotPassword>(sql, new { Validator = validator, SiteId = siteId }).FirstOrDefault();
            return isUsernameAvailable;
        }
        public int ResetPassword(string password, User user)
        {
            var salt = PasswordUtilities.GenerateSalt();
            var hash = PasswordUtilities.GenerateHash(password, salt);

            var sql = @"";

            var isUsernameAvailable = Query<int>(sql, new { Hash = hash, Salt = salt }).FirstOrDefault();
            return isUsernameAvailable;
        }
    }
}