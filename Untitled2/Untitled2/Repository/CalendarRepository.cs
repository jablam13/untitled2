﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Models.Calendar;

namespace Untitled2.Repository
{
    public class CalendarRepository : RepositoryBase
    {
        private readonly static int siteId = 5;

        public List<Event> GetEvents(int userId, int month, int year)
        {
            var sql = @"
SELECT et.Id, et.[Uid], et.SiteId, et.Name, et.[Description], et.StartTime, et.EndTime, et.Duration, et.CreatedDate, et.LastModifiedDate,
CASE WHEN EXISTS(SELECT Id FROM EventAttendees WHERE UserId = @UserId AND EventId = et.Id AND IsAttending = 1) THEN 1 ELSE 0 END AS IsAttending
FROM EventTable et
WHERE SiteId = @SiteId 
AND MONTH(StartTime) = @Month 
AND YEAR(StartTime) = @Year;;";

            var events = Query<Event>(sql, new { UserId = userId, SiteId = siteId, Month = month, Year = year }).ToList();

            return events;
        }

        public int AttendEvents(int userId, int eventId, int isAttending)
        {
            var sql = @"
IF NOT EXISTS(SELECT * FROM UserDetails u
JOIN EventAttendees ea on ea.UserId = u.Id
WHERE u.SiteId = @SiteId
AND u.Id = @UserId 
AND ea.EventId = @EventId)
BEGIN
	INSERT INTO EventAttendees
	SELECT NEWID(), @EventId, @UserId, CASE WHEN @IsAttending = 1 THEN 0 ELSE 1 END, GETDATE(),GETDATE()
END
ELSE 
BEGIN 
	UPDATE EventAttendees SET IsAttending = CASE WHEN @IsAttending = 1 THEN 0 ELSE 1 END WHERE EventId = @EventId AND UserId = @UserId
END;

SELECT IsAttending FROM EventAttendees WHERE UserId = @UserId AND EventId = @EventId;";

            var isGoing = Query<int>(sql, new { UserId = userId, SiteId = siteId, EventId = eventId, IsAttending = isAttending }).FirstOrDefault();


            return isGoing;
        }

        public Event AddEvent(int userId, Event addEvent)
        {
            var sql = @"
DECLARE @OutputEvent TABLE(Id INT);

IF(@EventName != '' OR @EventName != NULL)
BEGIN
	INSERT INTO EventTable
	OUTPUT INSERTED.Id INTO @OutputEvent(Id)
	SELECT NEWID(), @SiteId, @EventName, @EventDescription,@StartTime, @EndTime, NULL, GETDATE(), GETDATE()
END

IF EXISTS(SELECT Id FROM @OutputEvent)
BEGIN
	SELECT Id, [Uid], Name, [Description], StartTime, EndTime, Duration, CreatedDate, LastModifiedDate
	FROM EventTable WHERE Id = (SELECT Id FROM @OutputEvent);
END
";
            var addedEvent = Query<Event>(sql, new
            {
                UserId = userId,
                SiteId = siteId,
                EventName = addEvent.Name,
                EventDescription = addEvent.Description,
                StartTime = addEvent.StartTime,
                EndTime = addEvent.EndTime
            }).FirstOrDefault();

            return addedEvent;
        }
    }
}