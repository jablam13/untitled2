﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Untitled2.Models;
using Untitled2.Helpers;
using System.Web.Script.Serialization;


namespace Untitled2.Repository
{
    public class ToDoListRepository : RepositoryBase
    {
        private readonly static int siteId = 5;
        public List<ToDoList> GetLists(int? id = -1)
        {
            var sqlList = @"
SELECT l.*,  r.firstname + ' ' + r.lastname as Username 
FROM List l 
JOIN UserDetails u on u.Id = l.UserId 
JOIN Registrant r on r.Id = u.RegistrantId
WHERE l.active = 1 AND UserId =  @UserId
AND u.SiteId = @SiteId;";
            var sqlListItem = @"SELECT td.* FROM ToDoList td 
                                JOIN list l on l.id = td.ListId 
                                WHERE l.userid = @UserId 
                                    AND l.active = 1 
                                    AND td.active = 1 
                                    AND td.listid = @ListId 
                                    AND td.active = 1;";
            var lists = Query<ToDoList>(sqlList, new { UserId = id, SiteId = siteId }).ToList();
            foreach (var list in lists)
            {
                var listItems = Query<ToDoListItem>(sqlListItem, new { UserId = id, ListId = list.Id }).ToList();
                list.ListItems = listItems;
            }

            return lists;
        }

        public List<ToDoListItem> GetList(int userId, int id = -1)
        {
            var sqlListItem = @"
SELECT l.*,  r.firstname + ' ' + r.lastname as Username 
FROM List l 
JOIN UserDetails u on u.Id = l.UserId 
JOIN Registrant r on r.Id = u.RegistrantId
WHERE l.active = 1 AND UserId =  @UserId
AND u.SiteId = @SiteId;
AND l.Id = @ListId";
            //var list = Query<ToDoList>(sql, new { UserId = Current.User.Id}).FirstOrDefault();
            var listItems = Query<ToDoListItem>(sqlListItem, new { UserId = userId, ListId = id }).ToList();
            //list.ListItems = listItems;

            return listItems;
        }

        public List<ToDoListItem> GetListItems(int userId, int id = -1)
        {
            var sqlListItem = @"
SELECT td.* 
FROM ToDoList td 
JOIN list l on l.id = td.ListId 
WHERE l.userid = @UserId 
AND l.active = 1 
AND td.active = 1 
AND td.listid = @ListId;";
            //var list = Query<ToDoList>(sql, new { UserId = Current.User.Id}).FirstOrDefault();
            var listItems = Query<ToDoListItem>(sqlListItem, new { UserId = userId, ListId = id }).ToList();
            //list.ListItems = listItems;

            return listItems;
        }

        public ToDoListItem GetItem(int id = -1)
        {
            var sql = @"
SELECT Id as [Id], ListId as ListId, Title as Title, [Description] as [Description], CreatedDate as CreatedDate, LastModifiedDate as LastModifiedDate 
FROM ToDoList WHERE Id = @Id;";
            return Query<ToDoListItem>(sql, new { Id = id }).FirstOrDefault();
        }

        public ToDoList AddList(ToDoList list, int userId)
        {
            var sql = @"
DECLARE @OutputList TABLE(Id INT);
INSERT INTO List(UserId, Title, Description, Active, CreatedDate, LastModifiedDate) 
OUTPUT INSERTED.Id INTO @OutputList(Id)
VALUES(@UserId, @Title, @Description, 1, getdate(), getdate());

SELECT l.*,  r.Firstname + ' ' + r.Lastname as Username 
FROM List l 
JOIN UserDetails u ON u.Id = l.UserId 
JOIN Registrant r ON r.Id = u.RegistrantId
JOIN @OutputList o ON o.Id = l.Id
WHERE l.active = 1 AND UserId =  @UserId
AND u.SiteId = @SiteId;
";
            return Query<ToDoList>(sql, new { UserId = userId, Title = list.Title, Description = list.Description, SiteId = siteId }).FirstOrDefault();
        }

        public void UpdateList(List<ToDoList> lists, int userId)
        {
            var sql = @"UPDATE List SET Title = @Title, [Description] = @Description, LastModifiedDate = @LastModifiedDate WHERE Id = @Id AND UserId = @UserId";
            Execute(sql, lists.Select(o => new { Title = o.Title, Description = o.Description, LastModifiedDate = Convert.ToDateTime(o.LastModifiedDate), Id = o.Id, UserId = userId }));

        }

        public int AddItem(ToDoListItem item)
        {
            var sql = @"INSERT INTO ToDoList VALUES(@Title, @Description, getdate(), getdate(), 1, @ListId); SELECT SCOPE_IDENTITY()";
            return Query<int>(sql, new { Title = item.Title, Description = item.Description, ListId = item.ListId }).FirstOrDefault();
        }

        public int UpdateItem(List<ToDoListItem> list)
        {
            //This is sloppy code, but it works. Needs to be refactored.
            //var sql = @"";
            //int id = 0;
            //foreach (var i in list)
            //{
            //    sql = @"update ToDoList set Title = N'" + i.Title + 
            //            @"', [Description] = N'" + i.Description + "' where Id = " + i.Id;

            //    id += Execute(sql, list.Select(o => new { Title = o.Title, Description = o.Description, Id = o.Id }));

            //}
            //return id;


            //List<ToDoList> updateList = list.Select(o => new { Title = o.Title, Description = o.Description, Id = o.Id }) as List<ToDoList>;

            //Another way to Execute a list with Dapper
            //int id = 0;
            //list.ForEach(l =>
            //{
            //    var sql = @"update ToDoList set Title = @Title, [Description] = @Description, lastmodifieddate = getdate() where Id = @Id";
            //    id += Execute(sql, new { Title = l.Title, Description = l.Description, Id = l.Id });
            //});

            //An Even Simpler way to execute a list with Dapper. Dapper/SqlMapper loop the the list automatically, making 
            int id = 0;
            var sql = @"UPDATE ToDoList SET Title = @Title, [Description] = @Description, LastModifiedDate = GETDATE() WHERE Id = @Id AND ListId = @ListId";
            id += Execute(sql, list.Select(o => new { Title = o.Title, Description = o.Description, Id = o.Id, ListId = o.ListId }));

            return id;
        }

        public int RemoveItem(ToDoListItem item)
        {
            var sql = @"UPDATE ToDoList SET Active = 0, LastModifiedDate = GETDATE() WHERE Id = @Id AND ListId = @ListId;";
            return Query<int>(sql, new { Id = item.Id, ListId = item.ListId }).FirstOrDefault();
        }

        public int RemoveList(ToDoList list)
        {
            var sql = @"UPDATE List SET Active = 0, LastModifiedDate = GETDATE() WHERE Id = @Id";
            return Query<int>(sql, new { Id = list.Id }).FirstOrDefault();
        }
    }
}