﻿App.Register = (function () {
    function Register(user) {
        var self = this;
        self.FirstName = ko.observable(user.firstName).extend({
            required: true,
            minLength: 2,
            maxLength: 50
        });
        self.LastName = ko.observable(user.lastName).extend({
            required: true,
            minLength: 2,
            maxLength: 50
        });
        self.EmailAddress = ko.observable(user.emailAddress).extend({
            email: true,
            required: true,
            rateLimit: 1000
        });
        self.Password = ko.observable(user.password).extend({
            required: true,
            minLength: 6,
            message: "Password is required, with atleast 1 upper case character, 1 lowercase character, 1 number and 1 special character, ",
            maxLength: 50
        });
        self.PasswordRpt = ko.observable(user.passwordRpt).extend({
            required: true,
            areSame: self.Password
        });
        self.Errors = ko.observableArray(user.errors);

        var validator = $("form").validate();

        if (user.errors != null) {
            validator.showErrors(user.Errors);
        }
    }

    Register.prototype.submit = function (user) {
        var userInfo = {
            FirstName: this.FirstName(),
            LastName: this.LastName(),
            EmailAddress: this.EmailAddress(),
            Password: this.Password(),
            PasswordRpt: this.PasswordRpt()
        };
        if ($("#registrationForm").valid()) {
            App.postForm({ url: "account/register", data: userInfo });

            this.FirstName('');
            this.LastName('');
            this.Username('');
            this.Password('');
            this.PasswordRpt('');
        }
    };

    Register.prototype.reset = function () {
        this.FirstName('');
        this.LastName('');
        this.Username('');
        this.Password('');
        this.PasswordRpt('');
    };

    return Register;
})();
