﻿App.Event = (function () {
    function Event(event, day) {
        var self = this;
        self.Id = ko.observable(event.Id);
        self.Uid = ko.observable(event.Uid);
        self.Name = ko.observable(event.Name);
        self.Description = ko.observable(event.Description);
        self.SiteId = ko.observable(event.SiteId);
        self.StartTime = ko.observable(moment(event.StartTime).format("YYYY-MM-DD HH:mm"));
        self.EndTime = ko.observable(moment(event.EndTime).format("YYYY-MM-DD HH:mm"));
        self.Duration = ko.observable(event.Duration);
        self.IsAttending = ko.observable(event.IsAttending);
        self.IsAttendingText = ko.computed(function () {
            if (self.IsAttending() == 1) { return 'LEAVE' } else { return 'JOIN' }
        });
        self.Attendees = ko.observableArray(ko.utils.arrayMap(event.Attendees, function (attendee) {
            return new App.Event(attendee, self);
        }));
        self.Day = day;
        //Add Event Variables
    };

    Event.prototype.Attend = function () {
        var self = this;
        var event = {};
        event.eventId = self.Id();
        event.isAttending = self.IsAttending();

        $.ajax({
            type: 'POST',
            url: '/calendar/attend',
            contentType: 'application/json',
            data: JSON.stringify(event),
            dataType: 'json'
        }).done(function (data) {
                self.IsAttending(data)
        });
    };

    return Event;
})();

