﻿App.Day = (function () {
    function Day(day, calendar) {
        var self = this;
        self.Date = ko.observable(moment(day.Date).format("MMMM Do YYYY"));
        self.DayNumber = ko.observable(moment(day.Date).date());
        self.IsFirstDay = ko.observable(day.IsFirstDay);
        self.IsEmpty = ko.observable(day.IsEmpty);
        self.currentDay = ko.observable(day.currentDay);
        self.Events = ko.observableArray(ko.utils.arrayMap(day.Events, function (event) {
            return new App.Event(event, self);
        }));
        self.IsHighlighted = ko.observable(false);
        self.selectDay = function (d) {
            calendar.EventList({ selectedDay: d.Events(), Day: self });
        };
        self.IsADate = ko.computed(function () {
            return (!isNaN(parseInt(self.DayNumber())));
        });

        self.ShowAddEvent = ko.observable(false);
        self.addName = ko.observable('');
        self.addDescription = ko.observable('');
        self.addStartDate = ko.observable('');
        self.addEndDate = ko.observable('');
    };

    Day.prototype.AddEvent = function () {
        var self = this;

        if (self.Day.ShowAddEvent() == false) {
            self.Day.ShowAddEvent(true);
        } else {
            if (self.Day.IsADate() == true && self.Day.addName() != '' && self.Day.addStartDate() != '' && self.Day.addEndDate() != '' && self.Day.DayNumber) {
                var event = {};
                event.name = self.Day.addName();
                event.description = self.Day.addDescription();
                event.startTime = moment(self.Day.addStartDate()).format("YYYY-MM-DD HH:mm:ss.SSS");
                event.endTime = moment(self.Day.addEndDate()).format("YYYY-MM-DD HH:mm:ss.SSS");


                console.log(event);
                $.ajax({
                    type: 'POST',
                    url: '/calendar/addevent',
                    contentType: 'application/json',
                    data: JSON.stringify(event),
                    dataType: 'json'
                }).done(function (data) {
                    if (data != 0) {
                        self.Day.Events.push(new App.Event(event, self));
                    }
                });
            }
        }
    };
    Day.prototype.CancelAddEvent = function () {
        var self = this;

        self.Day.ShowAddEvent(false);
        self.Day.addName('');
        self.Day.addDescription('');
        self.Day.addStartDate('');
        self.Day.addEndDate('');
    };

    return Day;
})();