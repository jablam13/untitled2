﻿App.Calendar = (function () {
    function Calendar(calendar) {
        var self = this;
        self.Month = ko.observable(moment().format("YYYY-MM"));
        self.CurrentMonth = ko.observable(Date.today().getMonthNameString('en') + ' ' + Date.today().getFullYear('en'));
        self.MonthState = ko.observable(0);
        self.StartDay = ko.observable();
        self.Days = ko.observableArray(CreateCalendarArray(self.MonthState(), calendar.Days, self));
        self.EventList = ko.observable();
    };


    Calendar.prototype.previousMonth = function () {
        var self = this;
        var month = {};
        var monthString = self.Month().toString();
        var monthState = self.MonthState();

        if (monthState < 0) {
            month.date = moment(monthString).subtract(Math.abs(monthState) + 1, 'M');
        } else {
            month.date = moment(monthString).add(monthState - 1, 'M');
        }

        $.ajax({
            type: 'POST',
            url: '/calendar/getmonth',
            contentType: 'application/json',
            data: JSON.stringify(month),
            dataType: 'json'
        }).done(function (data) {
            self.MonthState(self.MonthState() - 1);
            self.CurrentMonth(Date.today().addMonths(self.MonthState()).getMonthNameString('en') + ' ' + Date.today().addMonths(self.MonthState()).getFullYear('en'));
            self.Days(CreateCalendarArray(self.MonthState(), data.Days, self));
        })
    };

    Calendar.prototype.nextMonth = function () {
        var self = this;
        var month = {};
        var monthString = self.Month().toString();
        var monthState = self.MonthState();

        if (monthState < 0) {
            month.date = moment(monthString).subtract(Math.abs(monthState) - 1, 'M');
        } else {
            month.date = moment(monthString).add(monthState + 1, 'M');
        }

        $.ajax({
            type: 'POST',
            url: '/calendar/getmonth',
            contentType: 'application/json',
            data: JSON.stringify(month),
            dataType: 'json'
        }).done(function (data) {
            self.MonthState(self.MonthState() + 1);
            self.CurrentMonth(Date.today().addMonths(self.MonthState()).getMonthNameString('en') + ' ' + Date.today().addMonths(self.MonthState()).getFullYear('en'));
            self.Days(CreateCalendarArray(self.MonthState(), data.Days, self));
        })
    };

    return Calendar;
})();

function CreateCalendarArray(monthState, days, koCalendar) {
    var calendar = [];
    var firstDay = Date.today().addMonths(monthState).clearTime().moveToFirstDayOfMonth();
    //var firstday = fd.toString("MM/dd/yyyy");
    var calendarDay = Date.today().addMonths(monthState).clearTime().moveToFirstDayOfMonth();
    var lastDay = Date.today().addMonths(monthState).clearTime().moveToLastDayOfMonth();
    var day = {};



    for (var currentDay = 0; currentDay <= 41; currentDay++) {
        day = {};
        day.currentDay = currentDay;
        calendarDay = Date.today().addMonths(monthState).clearTime().moveToFirstDayOfMonth();

        //is beginning of the week
        if (((currentDay) % 7) == 0) {
            day.IsFirstDay = 1;
        } else {
            day.IsFirstDay = 0;
        }

        if ((firstDay.getDay() <= (currentDay)) && ((currentDay - firstDay.getDay()) <= (lastDay.getDaysInMonth() - 1))) {
            day.IsEmpty = 0;
            day.Date = calendarDay.add(currentDay - firstDay.getDay()).days();
        } else {
            day.IsEmpty = 1;
            day.Date = null;
        }

        days.filter(function (d) {
            if (moment(day.Date).format("YYYY-MM-DD") == moment(d.Date).format("YYYY-MM-DD")) {
                day.Events = d.Events;
            }
        });

        calendar[currentDay] = new App.Day(day, koCalendar);
    }

    return calendar;
}