﻿App.Map = (function () {
    function Map() {
        var map;
        var service;
        var placeService;
        var myLatLong = new google.maps.LatLng(38.667803, -90.32056);
        var markers = [];
        var geocodeName;
        var currentLat;
        var currentLon;
        var lists;
        var infoWindow;
        var searchMarkers = [];
        var bounds;
        var searchResults;
        var searchItems = [];

        function clearOverlays(markersArray) {
            for (var i = 0; i < markersArray.length; i++) {
                markersArray[i].setVisible(false);
            }
            markersArray.length = 0;
        }

        function initialize() {
            bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                zoom: 17,
                center: myLatLong,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            service = new google.maps.places.AutocompleteService();
            geocodeName = new google.maps.Geocoder();

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(
                //get position
                function (position) {
                    currentLat = position.coords.latitude;
                    currentLon = position.coords.longitude;

                    myLatLong = new google.maps.LatLng(currentLat, currentLon);
                    map.setCenter(myLatLong);
                },
                function (error) {
                    alert('ouch');
                }, { 'enableHighAccuracy': true, 'timeout': 10000, 'maximumAge': 0 });
            }

            $('#divResults').append(myLatLong);

            infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(map, "dragend", function () {
                setAllMap(null);
                sidebar = document.getElementById('sidebar');
                while (sidebar.firstChild) sidebar.removeChild(sidebar.firstChild);
                $('#results').empty();

                //nearbySearch
                var request = {
                    bounds: map.getBounds(),
                    location: map.getBounds().getCenter(),
                    types: ['restaurant', 'bakery']
                };
                placeService = new google.maps.places.PlacesService(map);
                placeService.nearbySearch(request, GetPlaces);
                $('#listResults').hide(function () {
                    $('#searchResults').show();
                })
            });

            google.maps.event.addListener(map, "zoom_changed", function () {
            });

        }
        function GetPlaces(results, status, pagination) {
            if (results != null) {
                searchResults = results;
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    //window.setInterval(function () {
                    //    for (var i = 0; i < results.length; i++) {
                    //        createMarker(results[i])
                    //    }
                    //}, 3000);

                    var count = 0
                    var i = window.setInterval(function () {
                        if (results[count] != null) {
                            GetPlaceDetails(results[count]);
                        }

                        count++;
                        if (count == results.length) {
                            clearInterval(i);
                        }
                    }, 500);

                    if (pagination.hasNextPage) {
                        var moreButton = document.getElementById('more');

                        moreButton.disabled = false;

                        google.maps.event.addDomListenerOnce(moreButton, 'click',
                            function () {
                                moreButton.disabled = true;
                                pagination.nextPage();
                            });
                    }
                }
            }
            if (status == google.maps.places.PlacesServiceStatus.OVER_QUERY_LIMIT) {
                console.log('Careful! - nearbyCallback');
                var count = 0
                var i = window.setInterval(function () {
                    if (results[count] != null) {
                        //GetPlaceDetails(results[count]);
                    }

                    count++;
                    if (count == results.length) {
                        clearInterval(i);
                    }
                }, 1000);
            }
        }

        function GetPlaceDetails(ref) {
            var request = {
                placeId: ref.place_id
            };

            placeService.getDetails(request, function (place, status) {

                if (request != null) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        createMarker(place);
                    }
                    if (status == google.maps.places.PlacesServiceStatus.OVER_QUERY_LIMIT) {
                        console.log('OVER THE LINE!!');
                        createMarker(place);
                    }
                }
            });
        }

        function createMarker(place, status) {
            if (place != null) {

                if (typeof place.geometry !== 'undefined') { place.Lat = place.geometry.location.lat() }
                if (typeof place.geometry !== 'undefined') { place.Lng = place.geometry.location.lng() }

                var localLocation = new google.maps.LatLng(place.Lat, place.Lng);
                var i = 0,
                sidebar = document.getElementById('sidebar');

                var item = document.createElement('div');
                var itemDetails = document.createElement('div');

                var infoWindowContent = document.createElement('div');
                var infowindow = new google.maps.InfoWindow();

                //chItem.className = 'showMarkerInfo';
                item.innerHTML = place.name;
                item.marker = new google.maps.Marker({
                    map: map,
                    position: localLocation
                });
                item.onmouseover = function () { this.marker.setAnimation(google.maps.Animation.BOUNCE); };
                item.onmouseout = function () { this.marker.setAnimation(null); };
                sidebar.appendChild(item);

                var chItem = document.createElement('div');
                item.onclick = function () {
                    map.panTo(this.marker.getPosition());
                    this.marker.setAnimation(google.maps.Animation.BOUNCE);
                    if (chItem.className == 'showMarkerInfo') {
                        chItem.className = 'hideMarkerInfo';
                    } else if (chItem.className == 'hideMarkerInfo') {
                        $('.showMarkerInfo').removeClass('showMarkerInfo').addClass('hideMarkerInfo');
                        $('.addPlaceList').hide();
                        chItem.className = 'showMarkerInfo';
                    }
                };

                itemDetails.innerHTML = '';

                if (place.formatted_phone_number) {
                    itemDetails.innerHTML += '<div>' + place.formatted_phone_number + '</div>';
                }
                if (place.formatted_address) {
                    itemDetails.innerHTML += '<div>' + place.formatted_address + '</div>';
                }
                if (place.price_level) {
                    itemDetails.innerHTML += '<div>Price: ' + place.price_level + '</div>';
                }
                if (place.rating) {
                    itemDetails.innerHTML += '<div>Rating: ' + place.rating + '</div>';
                }
                if (place.website) {
                    itemDetails.innerHTML += '<div><a href="' + place.website + '" target="_blank" onClick="if(event.stopPropagation){event.stopPropagation();}event.cancelBubble=true;">' + place.website + '</a></div>';
                }

                itemDetails.className = 'itemDetails';

                chItem.appendChild(itemDetails);

                ListButtons(place, chItem);

                chItem.className = 'hideMarkerInfo';
                item.appendChild(chItem);
                item.className = 'searchItem'

                markers.push(item.marker);
                $('#results').append(place.name + ' - ' + place.price_level + '<br />');

                infoWindowContent.innerHTML = '<div>' + place.name + '</div>';

                ListButtons(place, infoWindowContent);

                google.maps.event.addListener(item.marker, 'click', function () {
                    map.panTo(item.marker.getPosition());
                    infowindow.setContent(infoWindowContent);
                    infowindow.open(map, this);
                });

                google.maps.event.addListener(map, "click", function (event) {
                    infowindow.close();
                });
            }
        }

        function ListButtons(obj, parentobj) {
            var o = document.createElement("div");
            var button = document.createElement("div");
            var selectMenu = document.createElement("div");

            o.appendChild(button);
            o.appendChild(selectMenu);

            if (typeof lists !== 'undefined') {
                $.each(lists, function (key, value) {
                    var selectMenuList = document.createElement("div");
                    selectMenuList.innerHTML = '<div class="listAdd">' + value.Name + '</div>';

                    selectMenuList.className = 'addPlaceList';
                    selectMenuList.onclick = function () {
                        updateList(value.Id, obj)
                        $('.addPlaceList').hide('slow');
                    }
                    selectMenu.appendChild(selectMenuList);
                });
            }
            button.innerHTML = '<div class="btnAddPlace" style="position: relative; text-align:center; left:0; padding: 5px 0px; width:100px; background-color:#cccccc">Add Place</div>';

            selectMenu.className = 'addPlaceListMenu';
            button.onclick = function (e) {
                e.stopPropagation();
                console.log($(this));
                $('.addPlaceList').show('slow');
            }

            parentobj.appendChild(o);
        }

        function updateList(listid, place) {
            var postData = {
                ListId: listid,
                Lat: place.Lat,
                Lng: place.Lng,
                Name: 'Name Entered',
                Description: 'This is a personalized description for an added place',
                GooglePlaceId: place.place_id,
                icon: place.icon,
                GoogleName: place.name,
                price_level: place.price_level,
                rating: place.rating,
                vicinity: place.vicinity,
                formatted_address: place.formatted_address,
                international_phone_number: place.international_phone_number,
                permanently_closed: place.permanently_closed,
                website: place.website
            }
            updatePostback(postData);
        }

        function updatePostback(postData) {
            var value = {};
            $.ajax({
                type: 'POST',
                url: '/maps/additem',
                contentType: 'application/json',
                data: JSON.stringify(postData),
                dataType: 'json'
            }).done(function (data, textStatus, xhr) {
                //Add List Item To List
                if (data !== null) {
                    var sortedList = lists
                       .filter(function (el) {
                           return el.Id == data.ListId;
                       });
                    console.log(sortedList);

                    var result = $.grep(sortedList[0].PlaceList, function (list) {
                        return list.GooglePlaceId == data.GooglePlaceId;
                    });
                    if (result.length == 0 && sortedList[0] != null) {
                        CreateListItem(data, sortedList[0]);
                    }
                }

                //$('#toDoList').append(JSON.stringify(data));
                console.log('Posted back Successfully');
            }).fail(function (xhr, textStatus, errorThrown) {
                //console.log(xhr.responseJSON.errors);
                console.log('Posted back UnSuccessfully');
            });
        }


        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
            markers = [];
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        (function () {

            window.onload = function () {
                GetLists();
            }
        })();

        function GetLists() {
            $.ajax({
                type: 'GET',
                url: '/maps/getlists',
                contentType: 'application/json',
                dataType: 'json'
            }).done(function (data, textStatus, xhr) {
                if (data.length != 0) {
                    HandleLists(data);
                    console.log(data)
                }
            }).fail(function (xhr, textStatus, errorThrown) {
                //console.log(xhr.responseJSON.errors);
                console.log('Posted back UnSuccessfully');
            });
        }

        function AppendListItem(value) {
            var list = document.createElement('div');
            var listClose = document.createElement('div');

            list.id = 'l' + value.Uid;
            list.innerHTML = value.Name;
            list.className = 'list';
            listClose.className = 'listClose';

            listClose.innerHTML = 'X';
            listClose.onclick = function () {
                console.log('close: ' + value.Id)
                if (confirm('Are you sure you want to remove this list?')) {
                    RemoveList(value.Id, value.Uid);
                    $('#l' + value.Uid).hide();
                }
            };
            list.appendChild(listClose);

            $('#lists').append(list);

        }

        function RemoveList(listId, listUid) {
            var postData = {}
            postData.ListId = listId;
            $.ajax({
                type: 'POST',
                url: '/maps/removelist',
                contentType: 'application/json',
                data: JSON.stringify(postData),
                dataType: 'json'
            }).done(function (data, textStatus, xhr) {
                $('#' + listId).remove();
                $('.l' + listId).remove();
                console.log($('.l' + listId))
                console.log('Posted back Successfully');
                lists = lists
                   .filter(function (el) {
                       return el.Id !== listId;
                   });
            }).fail(function (xhr, textStatus, errorThrown) {
                console.log('Posted back UnSuccessfully');
            });
        }

        function RemoveListItem(itemId, googleId, listId) {
            var postData = {}
            postData.Id = itemId;
            postData.GooglePlaceId = googleId;
            $.ajax({
                type: 'POST',
                url: '/maps/removePlace',
                contentType: 'application/json',
                data: JSON.stringify(postData),
                dataType: 'json'
            }).done(function (data, textStatus, xhr) {
                console.log(itemId)
                console.log(data)
                console.log(listId)
                //$('#li' + data.Uid).remove();
                $.each(lists, function (i, l) {
                    console.log(l.PlaceList);
                    l.PlaceList.filter(function (p) {
                        return p.GooglePlaceId !== data.GooglePlaceId;
                    });

                });
                console.log('Posted back Successfully');
            }).fail(function (xhr, textStatus, errorThrown) {
                console.log('Posted back UnSuccessfully');
            });
        }
        function HandleLists(d) {
            lists = d;
            $.each(lists, function (key, list) {

                AppendListItem(list);

                $.each(list.PlaceList, function (index, place) {
                    CreateListItem(place, list);
                });
            });
        }

        function CreateListItem(place, list) {
            var item = document.createElement('div');
            var itemDetails = document.createElement('div');
            var itemDetailSub = document.createElement('div');
            var itemClose = document.createElement('div');
            infowindow = new google.maps.InfoWindow();
            var loc = new google.maps.LatLng(place.Lat, place.Lng);
            item.innerHTML = '' + place.GoogleName;

            itemDetailSub.innerHTML = '';

            if (place.formatted_phone_number) {
                itemDetailSub.innerHTML += '<div>' + place.formatted_phone_number + '</div>';
            }
            if (place.formatted_address) {
                itemDetailSub.innerHTML += '<div>' + place.formatted_address + '</div>';
            }
            if (place.price_level) {
                itemDetailSub.innerHTML += '<div>Price: ' + place.price_level + '</div>';
            }
            if (place.rating) {
                itemDetailSub.innerHTML += '<div>Rating: ' + place.rating + '</div>';
            }
            if (place.website) {
                itemDetailSub.innerHTML += '<div><a href="' + place.website + '" target="_blank" onClick="if(event.stopPropagation){event.stopPropagation();}event.cancelBubble=true;">' + place.website + '</a></div>';
            }

            itemDetailSub.className = 'itemDetails';

            itemDetails.appendChild(itemDetailSub);


            item.id = 'li' + place.Uid;
            item.marker = new google.maps.Marker({
                map: map,
                position: loc,
                optimized: false
            });
            item.onmouseover = function () { this.marker.setAnimation(google.maps.Animation.BOUNCE); };
            item.onmouseout = function () { this.marker.setAnimation(null); };

            itemDetails.className = 'hideMarkerInfo';

            item.onclick = function () {
                map.panTo(this.marker.getPosition());
                this.marker.setAnimation(google.maps.Animation.BOUNCE);
                if (itemDetails.className == 'showMarkerInfo') { itemDetails.className = 'hideMarkerInfo'; }
                else if (itemDetails.className == 'hideMarkerInfo') {
                    $('.showMarkerInfo').removeClass('showMarkerInfo').addClass('hideMarkerInfo');
                    itemDetails.className = 'showMarkerInfo';
                }


            };

            item.className = "listItem l" + list.Uid;

            $('#listResults').append(item);

            markers.push(item.marker);

            itemClose.className = 'listItemClose';

            itemClose.innerHTML = 'X';
            itemClose.onclick = function () {
                console.log('close: ' + list.Id)
                if (confirm('Are you sure you want to remove this list?')) {
                    RemoveListItem(place.Id, place.GooglePlaceId, place.ListId);
                    $('#li' + place.Uid).hide();
                    console.log(markers)
                }
            };
            item.appendChild(itemClose);
            item.appendChild(itemDetails);


            google.maps.event.addListener(item.marker, 'click', function () {
                map.panTo(item.marker.getPosition());
                infowindow.setContent(place.GoogleName);
                infowindow.open(map, this);
            });
            bounds.extend(loc);
            map.fitBounds(bounds);
        }

        $(document).on('click', '.list', function () {
            var listId = $(this).attr('id');
            var selectedList = getObjects(lists, 'Uid', listId.substring(1));
            setAllMap(null);
            $('.listItem').hide();
            $('.' + listId).show();
            $('#searchResults').hide();
            $('#listResults').show();
            $.each(selectedList[0].PlaceList, function (index, place) {
                var loc = new google.maps.LatLng(place.Lat, place.Lng);
                var item = $('#' + 1);

                item.marker = new google.maps.Marker({
                    map: map,
                    position: loc
                });
                item.onmouseover = function () { this.marker.setAnimation(google.maps.Animation.BOUNCE); };
                item.onmouseout = function () { this.marker.setAnimation(null); };

                item.onclick = function () {
                    map.panTo(this.marker.getPosition());
                    this.marker.setAnimation(google.maps.Animation.BOUNCE);
                };

                markers.push(item.marker);

                google.maps.event.addListener(item.marker, 'click', function () {
                    map.panTo(item.marker.getPosition());
                    infowindow.setContent(place.GoogleName);
                    infowindow.open(map, this);
                    infowindow.onmouseover = function () { this.marker.setAnimation(google.maps.Animation.BOUNCE); };
                    infowindow.onmouseout = function () { this.marker.setAnimation(null); };
                });
                bounds.extend(loc);
                map.fitBounds(bounds);
            });
        });

        $(document).on('click', '#cancelAddList', function () {
            $('#addListFields').hide('slow');
        });

        $(document).on('click', '.buttonToggle', function () {
            if ($(this).attr('id') == 'btnSearch') {
                $('#listResults').hide();
                $('#searchResults').show();
            } else {
                $('#listResults').show();
                $('#searchResults').hide();
            }
        });

        function AddList(list) {
            $.ajax({
                type: 'POST',
                url: '/maps/addList',
                contentType: 'application/json',
                data: JSON.stringify(list),
                dataType: 'json'
            }).done(function (data, textStatus, xhr) {
                console.log('Posted back Successfully');
                AppendListItem(data);
                console.log(data)
                lists.push(data);
                while (sidebar.firstChild) sidebar.removeChild(sidebar.firstChild);
            }).fail(function (xhr, textStatus, errorThrown) {
                //console.log(xhr.responseJSON.errors);
                console.log('Posted back UnSuccessfully');
            });
        }
        $(document).ready(function () {
            initialize();
            console.log(lists);
            $("#commentForm").validate({
                errorElement: 'div',
                wrapper: 'div',
                errorPlacement: function (error, element) {
                    error.insertAfter(element); // default function
                }
            });
            $("#txtACSearch").autocomplete({
                source: function (request, response) {

                    var placePredictionRequest = { input: request.term, location: myLatLong, radius: 320935 }
                    console.log(placePredictionRequest);
                    service.getPlacePredictions(placePredictionRequest, function (predictions, status) {
                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            var textSearch = $("#txtACSearch").val();
                            //alert(textSearch);
                            var request = {
                                location: myLatLong,
                                radius: 3000,
                                query: textSearch
                            };
                            serviceTS = new google.maps.places.PlacesService(map);

                            serviceTS.textSearch(request, function (predictionsTS, status) {
                                //console.log(predictionsTS)
                                response($.map(predictionsTS, function (prediction, i) {
                                    //alert(prediction.formatted_address);
                                    return {
                                        search: 0,
                                        p: prediction,
                                        label: prediction.name + ', ' + prediction.formatted_address,
                                        value: prediction.name + ', ' + prediction.formatted_address,
                                        reference: prediction.reference,
                                    }
                                }));
                            });
                        } else {
                            response($.map(predictions, function (prediction, i) {
                                return {
                                    search: 1,
                                    p: prediction,
                                    label: prediction.description,
                                    value: prediction.description,
                                    reference: prediction.reference,
                                }
                            }))
                        };
                    });
                },
                select: function (event, ui) {
                    setAllMap(null);
                    if (ui.item.search == 1) {
                        placeService.getDetails(ui.item.p, function (place, status) {
                            autoResults(place.geometry.location, place);
                            return;
                        });
                    } else {
                        autoResults(ui.item.p.geometry.location, ui.item.p);
                        return;
                    }
                }
            });
        });


        function autoResults(location, place) {
            var placeLocation = new google.maps.LatLng(location.lat(), location.lng())

            var stringvalue = 'Latitude: <input type="text" value="' + location.lat()
                + '" />     Longitude: <input type="text" value="' + location.lng() + '" />';
            $("#value").append(stringvalue);

            marker = new google.maps.Marker({
                map: map,
                draggable: false
            });

            marker.setMap(map);
            marker.setPosition(placeLocation);
            map.setCenter(placeLocation);

            var ps = new google.maps.places.PlacesService(map);
            var request = { placeId: place.place_id };

            ps.getDetails(request, function (p, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    addInfoWindow(marker, infoWindow, p);
                } else {
                    console.log("addInfoWindow Geocoder failed due to: " + status);
                };
            });
        }

        function addInfoWindow(marker, infoWin, place) {
            var infoWindowContent = document.createElement('div');

            infoWin.setContent(infoWindowContent);

            google.maps.event.addListener(marker, 'click', function () {
                infoWin.open(map, marker);

                place.Lat = place.geometry.location.lat();
                place.Lng = place.geometry.location.lng();

                infoWindowContent.innerHTML = '<div>' + place.name + '</div><div>' + place.formatted_address + '</div>';
                ListButtons(place, infoWindowContent);
            });

            google.maps.event.addListener(map, 'click', function () {
                infoWin.close();
            });
            markers.push(marker);
        }
        $(document).on('click', '#addList', function () {

            if ($('#addListFields').css('display') == 'none') {
                $('.addListField').val('');
                $('#addListFields').show('slow');
            }
            if ($('#addListFields').css('display') != 'none') {
                var list = {};
                list.Name = $('#listName').val();
                list.Description = $('#listDescription').val();
                if ($("#commentForm").valid()) {
                    AddList(list);
                    $('#addListFields').hide();
                    $('.addListField').val('');
                }
            }
        });

        $(document).on('click', '#setCurentLocation', function () {
            console.log(myLatLong);
            if (myLatLong) {
                map.setCenter(myLatLong);
                map.setZoom(17);
            } else {
                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(
                    //get position
                    function (position) {
                        currentLat = position.coords.latitude;
                        currentLon = position.coords.longitude;

                        myLatLong = new google.maps.LatLng(currentLat, currentLon);
                        map.setCenter(myLatLong);
                        map.setZoom(17);
                    },
                    function (error) {
                        alert('ouch');
                    }, { 'enableHighAccuracy': true, 'timeout': 10000, 'maximumAge': 0 });
                }
            }
        });

        $(document).on('focus', ':input', function () {
            $(this).attr('autocomplete', 'on');
        });

        function getObjects(obj, key, val) {
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(getObjects(obj[i], key, val));
                } else if (i == key && obj[key] == val) {
                    objects.push(obj);
                }
            }
            return objects;
        }
    }

    Map.prototype.login = function () {

    };

    return Map;
})();
