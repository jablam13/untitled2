﻿App.Layout = (function () {
    function Layout(masterData) {
        this.userMenuIsVisible = ko.observable(true);
        this.prioritiesPerformanceIsVisible = ko.observable(false);
    };

    Layout.prototype.toggleUserMenu = function () {
        this.userMenuIsVisible(!this.userMenuIsVisible());
    };

    Layout.prototype.logoutClick = function () {
        App.postForm({ url: "account/logout", data: {} });
    };

    return Layout;
})();