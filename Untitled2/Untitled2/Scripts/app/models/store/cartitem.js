﻿App.CartItem = (function () {
    function CartItem(item, cart) {
        var self = this;
        self.Cart = cart;
        self.Id = ko.observable(item.Id);
        self.Uid = ko.observable(item.Uid);
        self.StoreItemId = ko.observable(item.StoreItemId);
        self.StoreItemUid = ko.observable(item.StoreItemUid);
        self.StoreId = ko.observable(item.StoreId);
        self.CategoryId = ko.observable(item.CategoryId);
        self.Name = ko.observable(item.Name);
        self.Description = ko.observable(item.Description);
        self.Price = ko.observable(item.Price);
        self.Quantity = ko.observable(item.Quantity);
        self.TotalQuantity = ko.observable(item.TotalQuantity);
        self.selectQuantity = ko.observableArray(TotalQuantity(item.TotalQuantity));
        self.selectedQuantity = ko.observable(item.Quantity);
        self.LargeImg = ko.observable(item.LargeImg);
        self.SmallImg = ko.observable(item.SmallImg);
        self.Active = ko.observable(item.Active);
        self.OutsideCart = ko.contextFor(document.getElementById("cart")).$root;
    }

    CartItem.prototype.updateItem = function () {
        var self = this, item = null;
        if (self.Quantity() != self.selectedQuantity()) {
            item = {};
            item.Id = self.Id();
            item.Uid = self.Uid();
            item.StoreId = self.StoreId();
            item.Quantity = self.selectedQuantity();
            item.Price = self.Price();
            item.CartTransaction = 'UPDATE';

            $.ajax({
                type: 'POST',
                url: '/cart/transaction',
                contentType: 'application/json',
                data: JSON.stringify(item),
                dataType: 'json'
            }).done(function (data) {
                if (data.Id != -2) {
                    var cartItem = data.CartItems[0];
                    self.Quantity(cartItem.Quantity);

                    self.Cart.TotalPrice(data.TotalPrice);
                    self.Cart.Quantity(data.Quantity);
                    self.OutsideCart.TotalPrice(data.TotalPrice);
                    self.OutsideCart.Quantity(data.Quantity);
                    //self.selectQuantity(TotalQuantity(cartItem.TotalQuantity));
                } else {
                    console.log(data)
                }
            });
        }
    };

    CartItem.prototype.removeItem = function () {
        var self = this, item = {};
        item.Id = self.Id();
        item.Uid = self.Uid();
        item.StoreId = self.StoreId();
        item.Quantity = self.selectedQuantity();
        item.Price = self.Price();
        item.CartTransaction = 'REMOVE';

        $.ajax({
            type: 'POST',
            url: '/cart/transaction',
            contentType: 'application/json',
            data: JSON.stringify(item),
            dataType: 'json'
        }).done(function (data) {
            if (data.Id != -2) {
                self.Cart.CartItems.remove(function (item) {
                    return item.Id == self.Id;
                });

                self.Cart.TotalPrice(data.TotalPrice);
                self.Cart.Quantity(data.Quantity);
                self.OutsideCart.TotalPrice(data.TotalPrice);
                self.OutsideCart.Quantity(data.Quantity);
            } else {
                console.log(data)
            }
        });

        //window.location.reload(true);
    };


    return CartItem;
})();

function TotalQuantity(quantity) {
    var quantityArray = []
    if (quantity < 0) {
        quantityArray[0] = 0
    } else {
        for (var i = 0; i <= quantity; i++) {
            quantityArray[i] = i;
        }
    }
    return quantityArray;
}