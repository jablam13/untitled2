﻿App.Cart = (function () {
    function Cart(cart) {
        var self = this;
        self.Id = ko.observable(cart.Id);
        self.Uid = ko.observable(cart.Uid);
        self.TotalPrice = ko.observable(cart.TotalPrice);
        self.Quantity = ko.observable(cart.Quantity);
        self.LastModifiedDate = ko.observable(cart.LastModifiedDate);
        self.ComputeTotalValue = ko.observable(cart.ComputeTotalValue);
        self.Errors = ko.observableArray(cart.Errors);
        self.CartItems = ko.observableArray(ko.utils.arrayMap(cart.CartItems, function (cartItem) {
            return new App.CartItem(cartItem, self);
        }));
        self.HasItems = ko.observable(self.CartItems().length);
    };

    return Cart;
})();