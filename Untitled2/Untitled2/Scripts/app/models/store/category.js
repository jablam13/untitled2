﻿App.Category = (function () {
    function Category(category, store) {
        var self = this;
        self.Id = ko.observable(category.Id);
        self.Uid = ko.observable(category.Uid);
        self.StoreId = ko.observable(category.StoreId);
        self.Name = ko.observable(category.Name);
        self.Description = ko.observable(category.Description);
        self.LargeImg = ko.observable(category.LargeImg);
        self.SmallImg = ko.observable(category.SmallImg);
        self.StoreItems = ko.observableArray(ko.utils.arrayMap(category.StoreItems, function (storeItem) {
            return new App.StoreItem(storeItem, self);
        }));
        self.Active = ko.observable(category.Active);
        self.selectCategory = function (category) {
            store.ItemList({ selectedCategory: category.StoreItems() });
        }
    }

    //Category.prototype.toggleUserMenu = function () {

    //};

    //Category.prototype.logoutClick = function () {

    //};

    return Category;
})();