﻿App.Checkout = (function () {
    function Checkout() {
        var self = this;
        self.Number = ko.observable();
        self.ExpMonth = ko.observable();
        self.ExpYear = ko.observable();
        self.CVS = ko.observable();
        self.SameAddress = ko.observable(true);

        //self.copyAddress = function () {
        //    var clean = ko.mapping.toJS(self.shippingAddress);
        //    ko.mapping.fromJS(clean, self.billingAddress);
        //};

        self.ShippingAddress = ko.observable(new App.Address());
        self.BillingAddress = ko.computed(function () {
            if (self.SameAddress() == true) {
                return self.ShippingAddress;
            } else {
                return new App.Address();
            }
        }, this);
    };

    Checkout.prototype.BuyItem = function () {
        var self = this; 
        console.log(self.ShippingAddress())
        console.log(self.BillingAddress())
    };

    Checkout.prototype.CancelBuy = function () {

    };

    return Checkout;
})();

App.Address = (function () {
    function Address() {
        var self = this;
        self.Name = ko.observable();
        self.Address1 = ko.observable();
        self.Address2 = ko.observable();
        self.City = ko.observable();
        self.State = ko.observable();
        self.ZipCode = ko.observable();
    };

    return Address;
})();

//App.CreditCard = (function () {
//    function CreditCard(cc) {
//        var self = this;
//        self.Number = ko.observable(cc.Name);
//        self.ExpMonth = ko.observable(cc.Address1);
//        self.ExpYear = ko.observable(cc.ExpYear);
//        self.CVS = ko.observable(cc.CVS);
//    };

//    return CreditCard;
//})();