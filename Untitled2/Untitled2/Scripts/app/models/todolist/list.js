﻿App.List = (function () {
    function List(list, lists) {
        var self = this;
        self.Lists = lists;
        self.Id = ko.observable(list.Id);
        self.UserId = ko.observable(list.UserId);
        self.Title = ko.observable(list.Title);
        self.Username = ko.observable(list.Username);
        self.Description = ko.observable(list.Description);
        self.ListItems = ko.observableArray(ko.utils.arrayMap(list.ListItems, function (i) { return new App.ListItem(i, self); }));
        self.listItemsVisible = ko.observable(false);
        self.showCancelItem = ko.observable(false);
        self.addItemTitle = ko.observable('');
        self.addItemDescription = ko.observable('');
        self.CreatedDate = ko.observable(list.CreatedDate);
        self.LastModifiedDate = ko.observable(list.LastModifiedDate);
        self.selectedList = ko.observable(false);
        self.selectedListColor = ko.computed(function () {
            if (self.listItemsVisible() == true) {
                return '#cccccc';
            } else {
                return '#ffffff';
            }
        })
    }

    //switch between lists;
    List.prototype.selectList = function (lists) {
        var self = this;
        ko.utils.arrayForEach(lists["lists"](), function (list) {
            (self.Id() == list.Id() ? list.listItemsVisible(true) : list.listItemsVisible(false));
        })
    }

    List.prototype.addItem = function () {
        var self = this;
        if (this.showCancelItem() == false) {
            this.showCancelItem(true);
        }
        else {
            postData = {
                Title: this.addItemTitle(),
                Description: this.addItemDescription(),
                ListId: this.Id()
            }
            self = this;
            $.ajax({
                type: 'POST',
                url: '/ToDoList/list/additem',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(postData)
            }).done(function (data, textStatus, xhr) {
                self.ListItems.push(new App.ListItem(data, self));
                self.showCancelItem(false);

                $('.todoItem').find('span').show();
                $('.todoItem').find('textarea').hide();
            }).fail(function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseJSON.errors);
            });
        }
    }
    List.prototype.cancelAdd = function () {
        this.showCancelItem(false);
        this.addItemTitle('');
        this.addItemDescription('');
    }

    List.prototype.updateProduct = function () {
        var postData = ko.utils.arrayMap(this.ListItems(), function (item) {
            return { Id: item.Id(), ListId: item.ListId(), Title: item.Title(), Description: item.Description(), CreatedDate: item.CreatedDate(), LastModifiedDate: item.LastModifiedDate() };
        });
        $.ajax({
            type: 'POST',
            url: '/ToDoList/List/UpdateListItems',
            contentType: 'application/json',
            data: JSON.stringify(postData),
            dataType: 'json'
        }).done(function (data, textStatus, xhr) {
            //$('#toDoList').append(JSON.stringify(data));
            console.log('Posted back Successfully');
        }).fail(function (xhr, textStatus, errorThrown) {
            //console.log(xhr.responseJSON.errors);
            console.log('Posted back UnSuccessfully');
        });
    }
    List.prototype.cancelUpdate = function (list) {
        self = this;
        var postData = {
            ListId: this.Id()
        }
        $.ajax({
            type: 'POST',
            url: '/ToDoList/List/CancelUpdateList',
            contentType: 'application/json',
            data: JSON.stringify(postData),
            dataType: 'json'
        }).done(function (data, textStatus, xhr) {
            //self.listItems = ko.observableArray(ko.utils.arrayMap(data, function (item) { return new App.ListItem(item); }));
            self.ListItems(ko.utils.arrayMap(data, function (item) { return new App.ListItem(item, self); }));
            $('.todoItem').find('span').show();
            $('.todoItem').find('textarea').hide();
        }).fail(function (xhr, textStatus, errorThrown) {
            console.log(xhr.responseJSON.errors);
        });
    }

    List.prototype.removeList = function () {
        self = this;
        var postData = {
            Id: this.Id()
        }
        console.log(self.Lists);
        self.Lists.lists.remove(self);
        $.ajax({
            type: 'POST',
            url: '/ToDoList/List/RemoveList',
            contentType: 'application/json',
            data: JSON.stringify(postData),
            dataType: 'json'
        }).done(function (data, textStatus, xhr) {
            self.Lists.lists.remove(self);
        }).fail(function (xhr, textStatus, errorThrown) {
            console.log(xhr.responseJSON.errors);
        });

    }

    return List;
})();
