﻿App.ListItem = (function () {
    function ListItem(item, list) {
        var self = this;
        self.List = list;
        self.Id = ko.observable(item.Id);
        self.ListId = ko.observable(item.ListId);
        self.Title = ko.observable(item.Title);
        self.Description = ko.observable(item.Description);
        self.CreatedDate = ko.observable(item.CreatedDate);
        self.LastModifiedDate = ko.observable(item.LastModifiedDate);
    }

    ListItem.prototype.removeItem = function (listItems) {
        var self = this;

        postData = {
            Id: this.Id(),
            ListId: this.ListId()
        }
        $.ajax({
            type: 'POST',
            url: '/ToDoList/List/RemoveItem',
            contentType: 'application/json',
            data: JSON.stringify(postData),
            dataType: 'json'
        }).done(function (data, textStatus, xhr) {
            console.log(self.List.ListItems)
            self.List.ListItems.remove(self);
        }).fail(function (xhr, textStatus, errorThrown) {
            console.log(xhr);
        });
        //self.List.ListItems.remove(self);
    }

    return ListItem;
})();


