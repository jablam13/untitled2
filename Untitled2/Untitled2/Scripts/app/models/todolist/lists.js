﻿App.Lists = (function () {
    function Lists(todoLists) {
        var self = this;
        self.lists = ko.observableArray(ko.utils.arrayMap(todoLists, function (item) { return new App.List(item, self); }));
        self.showCancelList = ko.observable(false);
        self.addListTitle = ko.observable('');
        self.addListDescription = ko.observable('');
        self.selectedList = ko.observable(false);
        self.selectedListColor = ko.computed(function () {
            return '#cccccc';
        });
    }

    Lists.prototype.addList = function () {
        if (this.showCancelList() == false) {
            this.showCancelList(true);
        }
        else {
            postData = {
                Title: this.addListTitle(),
                Description: this.addListDescription()
            }
            self = this;
            if (self.addListTitle().trim().length != 0 && self.addListDescription().trim().length != 0) {
                $.ajax({
                    type: 'POST',
                    url: '/ToDoList/list/addlist',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    traditional: true,
                    data: JSON.stringify(postData)
                }).done(function (data, textStatus, xhr) {
                    if (data != 0) {
                        self.lists.push(new App.List(data));
                        self.showCancelList(false);
                        self.addListTitle('');
                        self.addListDescription('');
                    }
                }).fail(function (xhr, textStatus, errorThrown) {
                    console.log(xhr);
                    console.log(textStatus);
                    console.log(errorThrown);
                });
            }
        }
    }

    Lists.prototype.cancelAddList = function () {
        this.showCancelList(false);
        this.addListTitle('');
        this.addListDescription('');
    }

    Lists.prototype.updateList = function () {
        var postData = ko.utils.arrayMap(this.lists(), function (list) {
            return { Id: list.Id(), Title: list.Title(), Description: list.Description(), LastModifiedDate: list.LastModifiedDate() };
        });
        $.ajax({
            type: 'POST',
            url: '/ToDoList/List/UpdateList',
            contentType: 'application/json',
            traditional: true,
            data: JSON.stringify(postData),
            dataType: 'json'
        }).done(function (data, textStatus, xhr) {
            $('#toDoList').append(JSON.stringify(data));
            console.log('Posted back Successfully');
        }).fail(function (xhr, textStatus, errorThrown) {
            //console.log(xhr.responseJSON.errors);
            console.log('Posted back UnSuccessfully');
        });
    }

    Lists.prototype.cancelUpdateList = function () {
        //self = this;
        ////console.log(ko.toJSON(this.listItems));
        //$.ajax({
        //    type: 'GET',
        //    url: '/ToDo/List/CancelUpdateList',
        //    contentType: 'application/json',
        //    dataType: 'json'
        //}).done(function (data, textStatus, xhr) {
        //    console.log(self.listItems());
        //    console.log(data);
        //    //self.listItems = ko.observableArray(ko.utils.arrayMap(data, function (item) { return new App.ListItem(item); }));
        //    self.listItems(ko.utils.arrayMap(data, function (item) { return new App.ListItem(item); }));
        //    console.log(self.listItems());
        //}).fail(function (xhr, textStatus, errorThrown) {
        //    console.log(xhr.responseJSON.errors);
        //});

        ////console.log(list.listItems());
        ////console.log('this.listItems === ' + ko.toJSON(this.listItems));
        ////this.listItems(list.listItems());
    }

    return Lists;
})();