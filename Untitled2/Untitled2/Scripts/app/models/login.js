﻿App.Login = (function () {
    function Login(loginData) {
        App.log(loginData);
        this.username = ko.observable(loginData.username).extend({
            email: true,
            required: true
        });;
        this.password = ko.observable(loginData.password).extend({
            required: true
        });;
        this.forgotEmail = ko.observable('').extend({
            email: true,
            required: true
        });
        this.userDoesntExist = ko.observable(false);

        var validator = $("form").validate();

        if (loginData.errors != null) {
            validator.showErrors(loginData.errors);
        }
    }


    Login.prototype.login = function () {
        if ($("form").valid()) {
            var formData = {
                username: this.username(),
                password: this.password()
            };
            App.postForm({ url: "account/login", data: formData });
        }
    };

    Login.prototype.cancel = function () {
        window.location.replace("../");
    };

    Login.prototype.remind = function () {
        var self = this;

        if (self.forgotEmail.isValid()) {
            formData = {
                Email: self.forgotEmail(),
                Validator: null
            };
            var response;
            UserExists(self.forgotEmail()).done(function (result) {
                self.userDoesntExist(!result)
                if (result === true) {
                    App.postForm({ url: "account/ResetPassword", data: formData });
                    console.log('run code');
                } else {
                    console.log('user doesnt exist');
                }
            });
        }
    };

    return Login;
})();

function UserExists(email) {
    var options = {};
    options.data = { Email: email };
    options.type = "POST";
    options.url = "account/userExists";
    return App.doAjax(options);
}