﻿
//auto-executing functions, Immediately Invoked Function Expressions, IIFE, auto anonymous functions
//http://blog.coolaj86.com/articles/how-and-why-auto-executing-function.html

var App = (function () {
    var App = function () { };

    App.prototype.configure = function (options) {
        this.options = options;
        var href = window.location.protocol + '//' + window.location.host;

        if (this.options.secureAppBase == '/') { this.options.secureAppBase = href + this.options.secureAppBase }
        if (this.options.appBase == '/') { this.options.appBase = href + this.options.appBase }

        $.validator.setDefaults({
            highlight: function (element, errorClass) {
                $(element).parents(".field").addClass("error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".field").removeClass("error");
            },
            errorPlacement: function (error, element) {

            }
        });

        $.validator.methods.requiredNotDefault = function (value, element, param) {
            var data = ko.dataFor(element)[param]();
            return data !== undefined;
        };
        $.validator.messages.requiredNotDefault = $.validator.messages.required;

        $.validator.addMethod("passwordComplexity", function (value) {
            var countCharacterRange = function (input, range) {
                var regex = new RegExp("[" + range + "]", "g");
                var remainingCharacters = value.replace(regex, "");
                return remainingCharacters == null ? 0 : remainingCharacters.length;
            };

            value = value == null ? "" : value;

            var lengthIsValid = value.length >= options.passwordComplexity.minLength;
            var uppercaseIsValid = countCharacterRange(value, "^A-Z") >= options.passwordComplexity.uppercaseCharacters;
            var lowercaseIsValid = countCharacterRange(value, "^a-z") >= options.passwordComplexity.lowercaseCharacters;
            var numberIsValid = countCharacterRange(value, "^0-9") >= options.passwordComplexity.numberCharacters;
            var specialIsValid = countCharacterRange(value, "a-zA-Z0-9") >= options.passwordComplexity.specialCharacters;

            var isValid = lengthIsValid && uppercaseIsValid && lowercaseIsValid && numberIsValid && specialIsValid;

            return isValid;
        }, function () {
            return (options.passwordComplexity.message || "This field must be at least {{minlength}} characters, contain at least {{lowercase}} lowercase letter, at least {{uppercase}} uppercase letter, at least {{number}} number, and {{special}} special character.")
                .replace("{{minlength}}", options.passwordComplexity.minlength)
                .replace("{{uppercase}}", options.passwordComplexity.uppercaseCharacters)
                .replace("{{lowercase}}", options.passwordComplexity.lowercaseCharacters)
                .replace("{{number}}", options.passwordComplexity.numberCharacters)
                .replace("{{special}}", options.passwordComplexity.specialCharacters);
        });
    };

    App.prototype.doAjax = function (options) {

        options = $.extend({}, { type: "GET", secure: false, data: {}, headers: {} }, options);

        // TODO add some validation here
        if (options.url[0] === "/") {
            options.url = options.url.substr(1);
        }

        if (options.type === "POST") {
            var antiForgeryTokenValue = $("input[name='__RequestVerificationToken']").val();
            options.headers["__RequestVerificationToken"] = antiForgeryTokenValue;
        }

        var xhr = $.ajax({
            dataType: "json",
            url: (options.secure ? this.options.secureAppBase : this.options.appBase) + options.url,
            contentType: "application/json",
            type: options.type,
            headers: options.headers,
            data: options.type === "POST" ? JSON.stringify(options.data) : options.data
        });
        return xhr.promise();
    };

    App.prototype.postForm = function (options) {
        var form = $("<form>").attr({
            method: "POST",
            action: (options.secure ? this.options.secureAppBase : this.options.appBase) + options.url,
        });
        var antiForgeryTokenValue = $("input[name='__RequestVerificationToken']").val();
        options.data["__RequestVerificationToken"] = antiForgeryTokenValue;

        _.forOwn(options.data, function (value, key) {
            form.append($("<input>").attr({ type: "hidden", name: key, value: value }));
        });
        form.appendTo("body").submit();
    };

    App.prototype.log = function () {
        try {
            console.log.apply(console, arguments);
        } catch (e) {
            try {
                console.log(arguments);
            } catch (e2) {

            }
        }
    };

    App.prototype.bindViewModel = function (options) {
        ko.applyBindings(options.viewModel, $("#" + options.elementId)[0]);
    };

    ko.bindingHandlers.toggle = {
        init: function (element, valueAccessor) {
            var value = valueAccessor();
            ko.applyBindingsToNode(element, {
                click: function () {
                    value(!value());
                }
            });
        }
    };

    ko.bindingHandlers.map = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var position = new google.maps.LatLng(allBindingsAccessor().latitude(), allBindingsAccessor().longitude());

            var marker = new google.maps.Marker({
                map: allBindingsAccessor().map,
                position: position,
                icon: 'Icons/star.png',
                title: name
            });

            viewModel._mapMarker = marker;
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var latlng = new google.maps.LatLng(allBindingsAccessor().latitude(), allBindingsAccessor().longitude());
            viewModel._mapMarker.setPosition(latlng);
        }
    };

    /*trimmed value binder for knockout*/
    ko.subscribable.fn.trim = function () {
        return ko.computed({
            read: function () {
                return this().trim();
            },
            write: function (value) {
                this(value.trim());
                this.valueHasMutated();
            },
            owner: this
        });
    };

    ko.validation.rules['areSame'] = {
        getValue: function (o) {
            return (typeof o === 'function' ? o() : o);
        },
        validator: function (val, otherField) {
            return val === this.getValue(otherField);
        },
        message: 'The fields must have the same value'
    };

    ko.validation.registerExtenders();

    ko.bindingHandlers.dateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            //initialize datepicker with some optional options
            var options = allBindingsAccessor().dateTimePickerOptions || {};
            $(element).datetimepicker(options);

            //when a user changes the date, update the view model
            ko.utils.registerEventHandler(element, "dp.change", function (event) {
                var value = valueAccessor();
                if (ko.isObservable(value)) {
                    if (event.date != null && !(event.date instanceof Date)) {
                        value(event.date.toDate());
                    } else {
                        value(event.date);
                    }
                }
            });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                var picker = $(element).data("DateTimePicker");
                if (picker) {
                    picker.destroy();
                }
            });
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var picker = $(element).data("DateTimePicker");
            //when the view model is updated, update the widget
            if (picker) {
                var koDate = ko.utils.unwrapObservable(valueAccessor());

                //in case return from server datetime i am get in this form for example /Date(93989393)/ then fomat this
                koDate = (typeof (koDate) !== 'object') ? new Date(parseFloat(koDate.replace(/[^0-9]/g, ''))) : koDate;

                picker.date(koDate);
            }
        }
    };

    ko.bindingHandlers["visibleInlineBlock"] = {
        "update": function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var isCurrentlyVisible = !(element.style.display == "none");
            if (value && !isCurrentlyVisible)
                element.style.display = "inline-block";
            else if ((!value) && isCurrentlyVisible)
                element.style.display = "none";
        }
    };

    return new App();

})();